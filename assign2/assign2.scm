(define (author)
    (println "AUTHOR: Cody Henderson cthenderson@crimson.ua.edu")
    )
(define old+ +)
(define old- -)
(define old* *)

;Start task 1
(define (loop f l)
    (define (iter i)
        (cond ((< i (car (cdr l)))
            (f i)
            (iter (old+ i 1)))
            )
        )
    (iter (car l))
    )

(define (run1)
    (loop (lambda (x) (inspect x)) '(0 5))
    (loop (lambda (x) (print (old+ x x))) '(10 20))
    (println "")
    (loop (lambda (x) (inspect x)) '(-1 5))
    (inspect (loop (lambda (x) (inspect x)) '(0 0)))
    )

;(run1)

;Start task 2
(define (curry f @)
    (define len (length (get 'parameters f)))
    (define providedLen (length @))
    (define old@ @)
    (define (genCurry arr)
        (lambda (@)
            (define old@ (append arr @))
            (define providedLen (length old@))
            (cond ((< providedLen len)
                (genCurry old@))
                (else (apply f old@))
                )
            )
        )
    (cond 
        ((< providedLen len)
            (genCurry @)
            )
        (else (apply f @))
        )
    )

(define (run2)
    (define (plus a b c) (old+ a b c))
    (inspect ((curry plus 1) 2 3))
    (inspect (((curry plus 1) 2) 3))
    (inspect ((curry plus) 1 2 3))
    (inspect (curry plus 1 2 3))
    )

;(run2)

;Start task 3
(define (infix->prefix e)
    ;Define stack with functions
    (define (createStack x)
        (lambda (op val)
            (cond ((eq? op 1) ;push
                    (createStack (cons val x))
                    )
                ((eq? op 2) ;pop
                    (createStack (cdr x)) 
                    )
                ((eq? op 3) ;peek
                    (car x)
                    )
                ((eq? op 4) ;isEmpty
                    (if (nil? x) #t #f)
                    )
                ((eq? op 5) ;show
                    x
                    )
                )
            )
        )
    ;Define stack helper functions
    (define (push l val) (l 1 val))
    (define (pop l) (l 2 nil))
    (define (peek l) (l 3 nil))
    (define (isEmpty l) (l 4 nil))
    (define (show l) (l 5 nil))
    ;Other helpers
    (define (isOperator x) (if (or (eq? x '+) (eq? x '-) (eq? x '*) (eq? x '/) (eq? x '^)) #t #f))
    (define (opPrec x) (cond ((or (eq? x '+) (eq? x '-)) 1) ((or (eq? x '*) (eq? x '/)) 2) ((eq? x '^) 3) (else -1)))
    ;Main iterator
    (define (iter eq ators ands looping)
            ;Done processing, return remaining operators and operands
        (cond ((nil? eq) (list ators ands))
            ;Not done processing
            (else
                (define tok (car eq))
                (cond
                    ;Need to loop backwards
                    (looping
                            ;Looping backwards to lesser precedence
                        (cond ((and (not (isEmpty ators)) (<= (opPrec tok) (opPrec (peek ators))))
                                (define op (peek ators))
                                (define r (peek ands))
                                (define l (peek (pop ands)))
                                (iter eq (pop ators) (push (pop (pop ands)) (list op l r)) #t)
                                )
                            ;Done looping. Return to normal
                            (else
                                (iter (cdr eq) (push ators tok) ands #f)
                                )
                            )
                        )
                    ;Just an operand
                    ((not (isOperator tok))
                        (iter (cdr eq) ators (push ands tok) #f)
                        )
                    ;Operator of greater precedence
                    ((or (isEmpty ators) (> (opPrec tok) (opPrec (peek ators))) (and (eq? tok '^) (eq? (peek ators) '^)))
                        (iter (cdr eq) (push ators tok) ands #f)
                        )
                    ;Operator of lesser precedence
                    ;Need to loop backwards
                    (else (iter eq ators ands #t))
                    )
                )
            )
        )
    (define res (iter e (createStack nil) (createStack nil) #f))
    (define ators (car res))
    (define ands (cadr res))
    ;Deal with the remaining operators
    (define (iter2 ators ands)
        (cond ((isEmpty ators) ands)
            (else
                (iter2 (pop ators) (push (pop (pop ands)) (list (peek ators) (peek (pop ands)) (peek ands))))
                )
            )
        )
    (car (show (iter2 ators ands)))
    )

(define (run3)
    (inspect (infix->prefix '(2 + 3 * x ^ 5 + a)))
    (inspect (infix->prefix '(2 * 3 + x)))
    (inspect (infix->prefix '(2 ^ 3 ^ 2)))
    (inspect (infix->prefix '(1 ^ 2 ^ 3 + 3 ^ 2 ^ 1)))
    (inspect (infix->prefix '(1 ^ 2 ^ 3 ^ 4)))
    (inspect (infix->prefix '(1 + 2 + 3 + 4)))
    (inspect (define a (quote (2 + 3 - 1 * 1 + 1))))
    (inspect (infix->prefix a))
    (inspect (eval (infix->prefix a) this))
    )

;(run3)

;Start task 4
(define (no-locals f)
    ;Helper functions
    (define (cadar a) (car (cdr (car a))))
    (define (caddar a) (car (cdr (cdr (car a)))))
    (define (caadar a) (car (cadar a)))
    (define (cdadar a) (cdr (cadar a)))
    (define (cdar a) (cdr (car a)))
    (define body (cddr f))
    (define (getLocals l)
        (cond ((not (pair? (car l))) nil) 
            ((not (eq? (caar l) 'define)) nil)
            ((pair? (cadar l))
                (cons (cons (caadar l) (append (list 'lambda (cdadar l)) (list (caddar l)))) (getLocals (cdr l)))
                )
            (else 
                (cons (cons (cadar l) (caddar l)) (getLocals (cdr l)))
                )
            )
        )
    (define (getBody l)
        (cond ((and (pair? (car l)) (eq? (caar l) 'define)) (getBody (cdr l)))
            (else 
                l
                )
            )
        )
    (define (buildParams p)
        (cond ((nil? p) nil)
            (else 
                (cons (caar p) (buildParams (cdr p)))
                )
            )
        )
    (define (buildArgs p)
        (cond ((nil? p) nil)
            (else 
                (cons (cdar p) (buildArgs (cdr p)))
                )
            )
        )
    (define locals (getLocals body))
    ;Build final answer
    (list 'define 
        (cadr f) 
        (append 
            (list (append (list 'lambda (buildParams locals)) (getBody body))) 
            (buildArgs locals)
            )
        )
    )
    
(define (run4)
    (define old (quote (define (nsq a) (define x (old+ a 1)) (define y (old- a 1)) (old* x y))))
    (inspect old)
    (define str (no-locals old))
    (inspect str)
    (eval str this)
    (inspect (nsq 4))    
    (define old2 (quote (define (nsr) 5)))
    (inspect old2)
    (define str2 (no-locals old2))
    (inspect str2)
    (eval str2 this)
    (inspect (nsr))
    (define old3 (quote (define (nss x) (define (a z) (* z 2)) (+ (a x) x))))
    (inspect old3)
    (define str3 (no-locals old3))
    (inspect str3)
    (eval str3 this)
    (inspect (nss 4))
    )
    
;(run4)

;Start task 5
(define (convert f)
    (define params (cadr f))
    (define body (cddr f))
    (define (iter par)
        (cond ((nil? par) body)
            ((nil? (cdr par))
                (cons 'lambda (cons (list (car par)) (iter (cdr par))))
                )
            (else 
                (cons 'lambda (cons (list (car par)) (cons (iter (cdr par)) nil)))
                )
            )
        )
    (iter params)
    )
    
(define (run5)
    (inspect (convert (quote (lambda (a b) (old+ a b)))))
    (define plus (eval (convert (quote (lambda (a b) (old+ a b)))) this))
    (inspect ((plus 3) 4))
    (inspect (convert (quote (lambda (a b c) (define d (old+ a b)) (old- c d)))))
    (define test (eval (convert (quote (lambda (a b c) (define d (old+ a b)) (old- c d)))) this))
    (inspect (((test 3) 4) 5))
    (inspect  (convert '(lambda (a b c) (lambda (d e) (+ a b c d e)))))
    (inspect (convert '(lambda (a b c) (+ a b) (- a c))))
    )
    
;(run5)

;Start task 6
(define (reverse* l)
    (define (cdar a) (cdr (car a)))
    (define (iter old new stack)
        (cond
            ;Done
            ((and (nil? old) (nil? stack)) new)
            ;Done at this level
            ;Go back to last level on stack
            ((nil? old)
                (iter 
                    (caar stack)
                    (cons new (cdar stack))
                    (cdr stack)
                    )
                )
            ;Found sublist
            ;Save current level to stack
            ((pair? (car old))
                (iter
                    (car old)
                    nil
                    (cons 
                        (cons (cdr old) new)
                        stack
                        )
                    )
                )
            ;Proceed normally
            (else
                (iter (cdr old) (cons (car old) new) stack)
                )
            )
        )
    (iter l nil nil)
    )

(define (run6)
    (define l1 (list 1 (list 2 (list 3 (list 4 5)))))
    (inspect (reverse* l1))
    (inspect (reverse* (list 1 2 3)))
    (define l2 '(1 (2 3 4 (5 6 (7))) 8 (9)))
    (inspect l2)
    (inspect (reverse* l2))
    (define l3 '(1 (2 (3) 4) 5))
    (inspect l3)
    (inspect (reverse* l3))
    (define l4 '((((1) 2) 3) 4))
    (inspect l4)
    (inspect (reverse* l4))
    )
    
;(run6)

;Start task 7
(define (accumulate op init seq)
    (if (null? seq)
        init
        (op (car seq) (accumulate op init (cdr seq)))
        )
    )
(define (accumulate-n op init seqs)
    (if (null? (car seqs))
        nil
        (cons (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))
        )
    )
(define (dot-product v w)
    (accumulate + 0 (map * v w))
    )
(define (matrix-*-vector m v)
    (map
        (lambda (row) (dot-product row v))
        (transpose m)
        )
    )
(define (transpose mat)
    (accumulate-n cons nil mat)
    )
(define (matrix-*-matrix m n)
    (map (lambda (col) (matrix-*-vector m col)) n)
    )
    
(define (run7)
    (inspect (matrix-*-vector (list (list 1 2 3) (list 4 5 6)) (list 1 2)))
    (println "Should be " '((9 12 15)))
    (inspect (transpose (list (list 1 2 3) (list 4 5 6))))
    (println "Should be " '((1 4) (2 5) (3 6)))
    (inspect (matrix-*-matrix (list (list 1 2 3) (list 4 5 6)) (list (list 1 2) (list 3 4))))
    (println "Should be " '((9 12 15) (19 26 33)))
    (inspect (matrix-*-matrix (list (list 1 3) (list 1 3)) (list (list 2 4) (list 2 4))))
    (println "Should be " '((6 18) (6 18)))    
    (inspect (matrix-*-matrix (list (list 1 1) (list 3 3)) (list (list 2 2) (list 4 4))))
    (println "Should be " '((8 8) (16 16)))
    (inspect (matrix-*-matrix (list (list 1 3) (list 3 1)) (list (list 2 4) (list 4 2))))
    (println "Should be " '((14 10) (10 14)))
    (inspect (matrix-*-matrix (list (list 1) (list 2) (list 3) (list 4)) (list (list 1 2 3 4))))
    (println "Should be " '((30)))
    (inspect (matrix-*-matrix (list (list 1 2 3 4)) (list (list 1) (list 2) (list 3) (list 4))))
    (println "Should be " '((1 2 3 4) (2 4 6 8) (3 6 9 12) (4 8 12 16)))
    )
    
;(run7)
    
;Start task 8
(define (node value left right)
    (define (display) (print value))
    this
    )
(define (displayTree root indent)
    (if (valid? root)
        (begin
            (displayTree (root'right) (string+ indent " "))
            (print indent)
            ((root'display))
            (println)
            (displayTree (root'left) (string+ indent " "))
            )
        )
    )
(define (insertInTree t val)
    (cond
        ((nil? t) (node val nil nil))
        ((< val (t'value))
            (node (t'value) (insertInTree (t'left) val) (t'right))
            )
        (else
            (node (t'value) (t'left) (insertInTree (t'right) val))
            )
        )
    )
    
(define (run8)
    (define t0 (node 5 nil nil))
    (displayTree t0 "  ")
    (println "--------------")
    (define t1 (insertInTree t0 2))
    (displayTree t1 "  ")
    (println "--------------")
    (define t2 (insertInTree t1 8))
    (displayTree t2 "  ")
    (println "--------------")
    (define (iIT t v) (insertInTree t v))
    (define t3 (iIT (iIT (iIT (iIT t2 7) 6) 1) 2))
    (displayTree t3 "  ")
    (println "--------------")
    )
    
;(run8)

;Start task 9
(define (big< a b)
    (define (check m n)
        (cond ((< (car m) (car n)) #t)
            ((> (car m) (car n)) #f)
            ((nil? (cdr m)) #f)
            (else (check (cdr m) (cdr n)))
            )
        )
    (cond
        ;Negative vs Positive
        ((and (eq? (car a) '-) (not (eq? (car b) '-)))
            #t
            )
        ;Positive vs Negative
        ((and (not (eq? (car a) '-)) (eq? (car b) '-))
            #f
            )
        (else
            (cond 
                ;Differing lengths
                ((or (and (eq? (car a) '-) (> (length a) (length b)))
                    (and (not (eq? (car a) '-)) (< (length a) (length b))))
                    #t
                    )
                ;Same length, compare digit by digit
                ((= (length a) (length b))
                    (check
                        (if (eq? (car a) '-) (cdr b) a)
                        (if (eq? (car a) '-) (cdr a) b)
                        )
                    )
                (else #f)
                )
            )
        )
    )
(define (big+ a b)
    (define (removeLeading0 a)
        (if (and (eq? (car a) 0) (not (nil? (cdr a)))) (removeLeading0 (cdr a)) a)
        )
    (cond
        ;One is empty
        ((nil? a) b)
        ((nil? b) a)
        ;Negative and Negative
        ((and (eq? (car a) '-) (eq? (car b) '-))
            (cons '- (big+ (cdr a) (cdr b)))
            ) 
        ;Positive and Negative
        ((and (not (eq? (car a) '-)) (eq? (car b) '-))
            (big- a (cdr b))
            )
        ;Negative and Positive
        ((and (eq? (car a) '-) (not (eq? (car b) '-)))
            (big- b (cdr a))
            )
        ;Positive and Positive
        (else
            (define ra (reverse* a))
            (define rb (reverse* b))
            (define (iter m n carry new)
                (cond ((and (nil? m) (nil? n)) (if (= carry 0) new (cons carry new)))
                    (else
                        (define digit (old+ (if (nil? m) 0 (car m)) (if (nil? n) 0 (car n)) carry))
                        (iter 
                            (if (nil? m) m (cdr m))
                            (if (nil? n) n (cdr n))
                            (/ digit 10)
                            (cons (% digit 10) new)
                            )
                        )
                    )
                )
            (removeLeading0 (iter ra rb 0 nil))
            )
        )
    )
(define (big- a b)
    (define (removeLeading0 a)
        (if (and (eq? (car a) 0) (not (nil? (cdr a)))) (removeLeading0 (cdr a)) a)
        )
    (cond
        ;Second number is bigger
        ((big< a b)
            (cons '- (big- b a))
            )
        ;Negative and Negative
        ((and (eq? (car a) '-) (eq? (car b) '-))
            (big- (cdr b) (cdr a))
            )
        ;Positive and Negative
        ((and (not (eq? (car a) '-)) (eq? (car b) '-))
            (big+ a (cdr b))
            )
        ;Negative and Positive
        ((and (eq? (car a) '-) (not (eq? (car b) '-)))
            (cons '- (big+ (cdr a) b))
            )
        ;Positive and Positive
        (else
            (define ra (reverse* a))
            (define rb (reverse* b))
            (define (iter m n stolen new isNeg)
                (cond ((and (nil? m) (nil? n))
                        (if (eq? isNeg 1) (cons '- new) new)
                        )
                    ((nil? n)
                        (iter (cdr m) n 0 (cons (old- (car m) stolen) new) isNeg)
                        )
                    (else
                        (define sub (old+ (car n) stolen))
                        (define step (old- (if (nil? m) 0 (car m)) sub))
                        (define (gotoPos x) (if (< x 0) (gotoPos (old+ x 10)) x))
                        (define (getStolen x i) (if (< x 0) (getStolen (old+ x 10) (old+ i 1)) i))
                        (define digit (gotoPos step))
                        (define stole (getStolen step 0))
                        (iter
                            (if (nil? m) m (cdr m))
                            (if (nil? n) n (cdr n))
                            stole
                            (cons digit new)
                            (if (nil? m) 1 isNeg)
                            )
                        )
                    )
                )
            (removeLeading0 (iter ra rb 0 nil 0))
            )
        )
    )
(define (big* a b)
    (define (max a b) (if (> a b) a b))
    (define (findXth a x) (if (or (= x 0) (nil? a)) a (findXth (cdr a) (old- x 1))))
    (define (firstXth a x new) (if (or (= x 0) (nil? a)) (reverse* new) (firstXth (cdr a) (old- x 1) (cons (car a) new))))
    (define (add0 a x) (if (= x 0) a (add0 (cons 0 a) (old- x 1))))
    (define (karat m n)
        (cond 
            ;Something is zero
            ((or (nil? m) (nil? n)) (list 0))
            ;Something is one digit, can just add
            ((or (= (length m) 1) (= (length n) 1))
                (define (smallbig* count addem val)
                    (if (> count 0)
                        (smallbig* (old- count 1) addem (big+ addem val))
                        val
                        )
                    )
                (reverse* (smallbig* 
                    (if (= (length m) 1) (car m) (car n))
                    (if (= (length m) 1) (reverse* n) (reverse* m))
                    (list 0)
                    ) )
                )
            (else
                (define ma (max (length m) (length n)))
                (define ma2 (/ ma 2))
                (define low1 (firstXth m ma2 nil))
                (define high1 (findXth m ma2))
                (define low2 (firstXth n ma2 nil))
                (define high2 (findXth n ma2))
                (define z0 (karat low1 low2))
                (define z1 (karat (reverse* (big+ (reverse* low1) (reverse* high1))) (reverse* (big+ (reverse* low2) (reverse* high2)))))
                (define z2 (karat high1 high2))
                (define stage1 (add0 z2 (old* 2 ma2)))
                (define stage2 (add0 (reverse* (big- (big- (reverse* z1) (reverse* z2)) (reverse* z0))) ma2))
                (define stage3 (big+ (reverse* stage1) (reverse* stage2)))
                (define stage4 (big+ stage3 (reverse* z0)))
                (reverse* stage4)
                )
            )
        )
    (cond
        ;Negative and Negative
        ((and (eq? (car a) '-) (eq? (car b) '-))
            (reverse* (karat (reverse* (cdr a)) (reverse* (cdr b))))
            )
        ;Negative and Positive
        ((and (eq? (car a) '-) (not (eq? (car b) '-)))
            (cons '- (reverse* (karat (reverse* (cdr a)) (reverse* b))))
            )
        ;Positive and Negative
        ((and (not (eq? (car a) '-)) (eq? (car b) '-))
            (cons '- (reverse* (karat (reverse* a) (reverse* (cdr b)))))
            )
        ;Positive and Positive
        (else
            (reverse* (karat (reverse* a) (reverse* b)))        
            )
        )
    )

(define (run9)
    (inspect (big+ (list 1 2 3) (list 4 5 6)))
    (inspect (big+ (list '- 1 2) (list '- 3 4)))
    (inspect (big+ (list '- 1 2) (list 3 4)))
    (inspect (big+ (list 3 4) (list '- 1 2)))
    (inspect (big+ (list 1 2) (list '- 3 4)))
    (inspect (big+ (list 1 2) (list 1 2 3 4)))
    (inspect (big+ (list 9 9) (list 9 9)))
    (inspect (big- (list 4 5 6) (list 1 2 3)))
    (inspect (big- (list 3 3) (list 1 0 0)))
    (inspect (big- (list '- 3 4) (list '- 1 2)))
    (inspect (big- (list '- 3 4) (list 1 2)))
    (inspect (big- (list 3 4) (list '- 1 2)))
    (inspect (big- (list '- 1 2) (list '- 3 4)))
    (inspect (big* (list 1 2) (list 1 2)))
    (inspect (big* (list 9 9) (list 9 9)))
    (inspect (big* (list 1 2 3) (list 1 2 3)))
    (inspect (big* (list 1 2 3 4) (list 4 5)))
    (inspect (big* (list 1 2 3 4) (list 5 6 7 8)))
    (inspect (big* (list '- 3 4) (list '- 5 6)))
    (inspect (big* (list '- 2 8) (list 9 5)))
    (inspect (big* (list 3 7) (list '- 8 1)))
    (inspect (big* (list 3 0 0 0 0 0) (list 2 0 0 0 0 0 0 0 0 0)))
    (inspect (big* (list 1 4 2 1 5 6 2 3 4 5) (list 2 1 3 4 4 4 1 2 3 5 6 5 4 1)))
    (inspect (big* (list 2 0 0 0 0 0) (list 1 0)))
    )

;(run9)

;Start task 10
(define MAXINT (old- (^ 2 15) 1))
(define MININT (old- 0 (^ 2 15)))
(define (normToBig x)
    (define (iter rem new)
        (if (eq? rem 0)
            new
            (iter (/ rem 10) (cons (% rem 10) new))
            )
        )
    (if (positive? x)
        (iter x nil)
        (cons '- (iter (abs x) nil))
        )
    )
(define (bigToNorm x)
    (define (iter rem new mult)
        (if (nil? rem)
            new
            (iter (cdr rem) (old+ new (old* (car rem) mult)) (old* mult 10))
            )
        )
    (if (eq? (car x) '-)
        (old- 0 (iter (reverse* (cdr x)) 0 1))
        (iter (reverse* x) 0 1)
        )
    )
(define MAXBIG (normToBig MAXINT))
(define MINBIG (normToBig MININT))
(define (reduceIfReq x)
    (if (and (big< x MAXBIG) (big< MINBIG x))
        (bigToNorm x)
        x)
    )
(define (+ a b)    
    (cond
        ;Real numbers default to old
        ((or (real? a) (real? b)) (old+ a b))
        ;Otherwise, two ints might need BigInt
        ((and (not (pair? a)) (not (pair? b)))
            ;Check if it will overflow
            (cond
                ((and (negative? a) (negative? b))
                    (define checkNum (old- MININT a))
                    (cond
                        ((> checkNum b)
                            (reduceIfReq (big+ (normToBig a) (normToBig b)))
                            )
                        (else (old+ a b))
                        )
                    )
                ((and (positive? a) (positive? b))
                    (define checkNum (old- MAXINT a))
                    (cond
                        ((< checkNum b)
                            (reduceIfReq (big+ (normToBig a) (normToBig b)))
                            )
                        (else (old+ a b))
                        )
                    )
                (else (old+ a b))
                )
            )
        ;If either are BigInt, use BigInt
        (else
            (reduceIfReq (big+
                (if (pair? a) a (normToBig a))
                (if (pair? b) b (normToBig b))
                ))
            )
        )
    )
(define (- a b)    
    (cond
        ;Real numbers default to old
        ((or (real? a) (real? b)) (old- a b))
        ;Otherwise, two ints might need BigInt
        ((and (not (pair? a)) (not (pair? b)))
            ;Check if it will overflow
            (cond
                ((and (negative? a) (positive? b))
                    (define checkNum (old- MININT a))
                    (cond
                        ((> checkNum (old- 0 b))
                            (reduceIfReq (big- (normToBig a) (normToBig b)))
                            )
                        (else (old- a b))
                        )
                    )
                ((and (positive? a) (negative? b))
                    (define checkNum (old- MAXINT a))
                    (cond
                        ((< checkNum (abs b))
                            (reduceIfReq (big- (normToBig a) (normToBig b)))
                            )
                        (else (old- a b))
                        )
                    )
                (else (old- a b))
                )
            )
        ;If either are BigInt, use BigInt
        (else
            (reduceIfReq (big-
                (if (pair? a) a (normToBig a))
                (if (pair? b) b (normToBig b))
                ))
            )
        )
    )
(define (* a b)
    (cond
        ;Real numbers default to old
        ((or (real? a) (real? b)) (old* a b))
        ;If either is a regular int 0, return 0
        ((or (and (not (pair? a)) (eq? a 0)) (and (not (pair? b)) (eq? b 0))) 0)
        ;Otherwise, two ints might need BigInt
        ((and (not (pair? a)) (not (pair? b)))
            ;Check if it will overflow
            (define checkHigh (/ MAXINT a))
            (define checkLow (/ MININT a))
            (if (or (< checkHigh b) (> checkLow b))
                (reduceIfReq (big* (normToBig a) (normToBig b)))
                (old* a b)
                )
            )
        ;If either are BigInt, use BigInt
        (else
            (reduceIfReq (big*
                (if (pair? a) a (normToBig a))
                (if (pair? b) b (normToBig b))
                ))
            )
        )
    )

(define (run10)
    (inspect MAXINT)
    (inspect MAXBIG)
    (inspect (+ 1 2))
    (inspect (+ 1.0 2.2))
    (inspect (+ (list 1) (list 2)))
    (inspect (+ (^ 2 14) (^ 2 14)))
    (inspect (+ (list '- 1 2) 12))
    (inspect (- 1 2))
    (inspect (- 1.0 2.2))
    (inspect (- (list 2) (list 1)))
    (inspect (- (- 0 (^ 2 14)) (^ 2 14)))
    (inspect (- (^ 2 14) (- 0 (^ 2 14))))
    (inspect (- 12 (list '- 1 2)))
    (inspect (* 2 3))
    (inspect (* 2.0 3.3))
    (inspect (* (list 2) (list 3)))
    (inspect (* (^ 2 14) 2))
    (inspect (* (^ 2 14) -2))
    (inspect (* (list '- 12) 12))
    )
    
;(run10)

(println "assignment 2 loaded!")
