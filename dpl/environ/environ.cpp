/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * environ.cpp
 */

#include "environ.h"
#include "../shared/cell.h"
#include "../shared/types.h"
#include "../eval/evalHelper.h"
#include <string.h>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>

using namespace Shared;

namespace Environment
{
    char ENV[] = "ENV";
    char JOIN[] = "JOIN";
    
    Cell* createEnv()
    {
        return extendEnv(0, 0, 0);
    }
    
    Cell* extendEnv(Cell* vars, Cell* vals, Cell* env)
    {
        return cons(Types::ENV, env, cons(Types::JOIN, vars, cons(Types::JOIN, vals, 0)));
    }
    
    void insert(char* id, Cell* val, Cell* env)
    {
        Cell* vars = env->cdr()->car();
        Cell* vals = env->cdr()->cdr()->car();
        env->cdr()->setcar(new Cell(-1, Types::JOIN, new Cell(-1, Types::VARDEF, id), vars));
        env->cdr()->cdr()->setcar(new Cell(-1, Types::JOIN, val, vals));
    }
    
    Cell* lookup(char* id, Cell* env)
    {
        Cell* vars = env->cdr()->car();
        Cell* vals = env->cdr()->cdr()->car();
        while (vars != 0)
        {
            if (strcmp(id, vars->car()->svalue) == 0)
                return vals->car();
            vars = vars->cdr();
            vals = vals->cdr();
        }
        if (env->car() != 0)
            return lookup(id, env->car());
        else
        {
        	char message[100];
        	sprintf(message, "Could not find object \"%s\" in the environment - line %d", id, Evaluator::EvalHelper::lineNum);
        	throw std::runtime_error(message);
        }
    }
    
    Cell* set(char* id, Cell* val, Cell* env)
    {
        Cell* vars = env->cdr()->car();
        Cell* vals = env->cdr()->cdr()->car();
        while (vars != 0)
        {
            if (strcmp(id, vars->car()->svalue) == 0)
            {
                vals->setcar(val);
                return val;
            }
            vars = vars->cdr();
            vals = vals->cdr();
        }
        if (env->car() != 0)
            return set(id, val, env->car());
        else
            return 0;
    }
    
    /*void displayLocalEnv(Cell* env)
    {
        cout << "Environment at " << env << "\n";
        Cell* vars = env->cdr()->car();
        Cell* vals = env->cdr()->cdr()->car();
        while (vars != 0)
        {
            cout << vars->car()->svalue << ":  ";
            if (vals->car()->svalue != 0)
                cout << vals->car()->svalue;
            else if (vals->car()->cvalue != 0)
                cout << "Cell Structure at " << vals->car()->cvalue;
            else
                cout << vals->car()->ivalue;
            cout << "\n";
        }
    }
    void displayEnv(Cell* env)
    {
        if (env != 0)
        {
            displayLocalEnv(env);
            displayEnv(env->car());
        }
    }*/
    
    /*Cell* cons(char* argType, Cell* l, Cell* r)
    {
        return new Cell(argType, l, r);
    }*/
}
