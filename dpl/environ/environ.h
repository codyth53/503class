/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * environ.h
 */

#ifndef ENVIRON
#define ENVIRON

namespace Shared
{
	class Cell;
}
using Shared::Cell;

namespace Environment
{
    Cell* createEnv();
    Cell* extendEnv(Cell* vars, Cell* vals, Cell* env);
    void insert(char* id, Cell* val, Cell* env);
    Cell* lookup(char* id, Cell* env);
    Cell* set(char* id, Cell* val, Cell* env);
    
    //void displayLocalEnv(Cell* env);
    //void displayEnv(Cell* env);
    
    //Cell* cons(char* argType, Cell* l, Cell* r);
}

#endif
