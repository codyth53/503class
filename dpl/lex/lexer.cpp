/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * lexer.cpp
 */

#include "lexer.h"
#include "lexeme.h"
#include "../shared/types.h"
#include <string.h>
#include <stdlib.h>

#define MAX_SIZE 300

using namespace Shared;

namespace Lex
{
    char Lexer::hold = 0;
    //FILE* Lexer::file;
    std::ifstream Lexer::file;
    int Lexer::lineNo;
    
    void Lexer::lexerInit(char* fileName)
    {
    	if (file.is_open())
    		file.close();
        //file = fopen(fileName, "r");
        file.open(fileName, std::ios::in);
        lineNo = 1;
    }
    
    Lexeme* Lexer::lex()
    {
        skipWhiteSpace();
        if (file.eof())
            return new Lexeme(Types::EOFT, lineNo);
        char ch = getCharacter();
        if (ch != '<')
            return new Lexeme(Types::UNKNOWN, lineNo);
        
        ch = getCharacter();
        if (ch == '!')
        {
            if (getCharacter() == '-' && getCharacter() == '-')
            {
                skipComment();
                return lex();
            }
            else
                return new Lexeme(Types::UNKNOWN, lineNo);
        }
        else if (ch == '/')
            return getCloseTag();
        else if (ch == '"')
            return getString();
        else if (isNumeric(ch) || ch == '-')
            return getNumber(ch);
        else if (isValidChar(ch))
            return getTag(ch);
        else
            return new Lexeme(Types::UNKNOWN, lineNo);
    }
    
    char Lexer::getCharacter()
    {
        if (hold != 0)
        {
            char temp = hold;
            hold = 0;
            return temp;
        }
        char ch;
        file.get(ch);
        return ch;
    }
    void Lexer::pushback(char ch)
    {
        hold = ch;
    }
    
    void Lexer::skipWhiteSpace()
    {
        char ch = getCharacter();
        while ((ch == ' '
                || ch == 9 //tab
                || ch == 10 //nl
                || ch == 12 //np
                || ch == 13) //cr
                && !file.eof()
                )
        {
            if (ch == 10)
                lineNo++;
            ch = getCharacter();
        }
        pushback(ch);
    }
    void Lexer::skipComment()
    {
        char ch1 = getCharacter();
        char ch2 = getCharacter();
        char ch3 = getCharacter();
        while (!(ch1 == '-' && ch2 == '-' && ch3 == '>') && !file.eof())
        {
            ch1 = ch2;
            ch2 = ch3;
            ch3 = getCharacter();
        }
    }
    
    int Lexer::isNumeric(char ch)
    {
        if (ch >= '0' && ch <= '9')
            return 1;
        else
            return 0;
    }
    int Lexer::isValidChar(char ch)
    {
        if ((ch >= '0' && ch <= '9')
            || (ch >= 'A' && ch <= 'Z')
            || (ch >= 'a' && ch <= 'z'))
            return 1;
        else
            return 0;
    }
    /*int Lexer::isWhiteSpace(char ch)
    {
        if (ch == ' ')
            return 1;
        else
            return 0;
    }*/
    char* Lexer::getStringUntilChar(char stop, char first)
    {
        int size = 2;
        int index = 0;
        char* str = (char*)malloc(sizeof(char)*2);
        if ((int)first != 0)
        {
            *str = first;
            index++;
        }
        char ch = getCharacter();
        while (ch != stop)
        {
            if (ch == '\\')
                ch = getCharacter();
            if (index+1 == size)
            {
                *(str+index) = 0;
                char* old = str;
                size *= 2;
                str = (char*)malloc(sizeof(char)*size);
                strncpy(str, old, size);
                free(old);
            }
            *(str+index) = ch;
            index++;
            ch = getCharacter();
        }
        *(str+index) = 0;
        return str;
    }
    
    Lexeme* Lexer::getString()
    {
        char* val = getStringUntilChar('"', 0);
        if (getCharacter() == '/' && getCharacter() == '>')
            return new Lexeme(Types::STRING, val, lineNo);
        else
            return new Lexeme(Types::UNKNOWN, lineNo);
    }
    Lexeme* Lexer::getNumber(char ch)
    {
        long int val = 0;
        int multiplier = 1;
        if (ch == '-')
        {
            multiplier = -1;
            ch = getCharacter();
        }
        if (isNumeric(ch) != 1)
            return new Lexeme(Types::UNKNOWN, lineNo);
        char* extra;
        /*val *= strtoi(ch, &extra, 10);
        ch = getCharacter();*/
        while (isNumeric(ch) && ch != '/')
        {
            char num[2];
            num[0] = ch;
            num[1] = 0;
            val *= 10;
            val += (strtol(num, &extra, 10) * multiplier);
            ch = getCharacter();
        }
        if (ch == '/' && getCharacter() == '>')
            return new Lexeme(Types::INTEGER, val, lineNo);
        else
            return new Lexeme(Types::UNKNOWN, lineNo);
    }
    Lexeme* Lexer::getTag(char ch)
    {
        char* tag = getStringUntilChar('>', ch);
        int length = strlen(tag);
        if (*(tag+length-1) == '/')
        {
            char* val = (char*)malloc(sizeof(char)*length);
            strncpy(val, tag, length-1);
            *(val+length-1) = 0;
            return new Lexeme(Types::ID, val, lineNo);
        }
        return new Lexeme(recognizeTag(tag, 1), tag, lineNo);
    }
    Lexeme* Lexer::getCloseTag()
    {
        char* tag = getStringUntilChar('>', 0);
        return new Lexeme(recognizeTag(tag, 0), tag, lineNo);
    }
    char* Lexer::recognizeTag(char* tag, int isOpenTag)
    {
        int length = strlen(tag);
        switch (length)
        {
            case 2: if (strncmp(tag, "if", 2) == 0)
                        return (isOpenTag) ? Types::OIF : Types::CIF;
                    else if (strncmp(tag, "eq", 2) == 0)
                        return (isOpenTag) ? Types::OEQ : Types::CEQ;
                    else if (strncmp(tag, "lt", 2) == 0)
                        return (isOpenTag) ? Types::OLT : Types:: CLT;
                    else if (strncmp(tag, "gt", 2) == 0)
                        return (isOpenTag) ? Types::OGT : Types::CGT;
                    else if (strncmp(tag, "le", 2) == 0)
                        return (isOpenTag) ? Types::OLE : Types::CLE;
                    else if (strncmp(tag, "ge", 2) == 0)
                        return (isOpenTag) ? Types::OGE : Types::CGE;
                    else if (strncmp(tag, "ne", 2) == 0)
                        return (isOpenTag) ? Types::ONE : Types::CNE;
                    else if (strncmp(tag, "or", 2) == 0)
                        return (isOpenTag) ? Types::OOR : Types::COR;
                    break;
            case 3: if (strncmp(tag, "not", 3) == 0)
                        return (isOpenTag) ? Types::ONOT : Types::CNOT;
                    else if (strncmp(tag, "and", 3) == 0)
                        return (isOpenTag) ? Types::OAND : Types::CAND;
                    else if (strncmp(tag, "def", 3) == 0)
                        return (isOpenTag) ? Types::ODEF : Types::CDEF;
                    else if (strncmp(tag, "get", 3) == 0)
                        return (isOpenTag) ? Types::OGET : Types::CGET;
                    else if (strncmp(tag, "set", 3) == 0)
                        return (isOpenTag) ? Types::OSET : Types::CSET;
                    break;
            case 4: if (strncmp(tag, "call", 4) == 0)
                        return (isOpenTag) ? Types::OCALL : Types::CCALL;
                    else if (strncmp(tag, "loop", 4) == 0)
                        return (isOpenTag) ? Types::OLOOP : Types::CLOOP;
                    else if (strncmp(tag, "cond", 4) == 0)
                        return (isOpenTag) ? Types::OCOND : Types::CCOND;
                    else if (strncmp(tag, "plus", 4) == 0)
                        return (isOpenTag) ? Types::OPLUS : Types::CPLUS;
                    else if (strncmp(tag, "else", 4) == 0)
						return (isOpenTag) ? Types::OELSE : Types::CELSE;
                    break;
            case 5: if (strncmp(tag, "block", 5) == 0)
                        return (isOpenTag) ? Types::OBLOCK : Types::CBLOCK;
                    else if (strncmp(tag, "param", 5) == 0)
                        return (isOpenTag) ? Types::OPARAM : Types::CPARAM;
                    else if (strncmp(tag, "minus", 5) == 0)
                        return (isOpenTag) ? Types::OMINUS : Types::CMINUS;
                    else if (strncmp(tag, "array", 5) == 0)
                        return (isOpenTag) ? Types::OARRAY : Types::CARRAY;
                    else if (strncmp(tag, "delay", 5) == 0)
                        return (isOpenTag) ? Types::ODELAY : Types::CDELAY;
                    else if (strncmp(tag, "ptreq", 5) == 0)
                        return (isOpenTag) ? Types::OPTREQ : Types::CPTREQ;
                    break;
            case 6: if (strncmp(tag, "return", 6) == 0)
                        return (isOpenTag) ? Types::ORETURN : Types::CRETURN;
                    else if (strncmp(tag, "divide", 6) == 0)
                        return (isOpenTag) ? Types::ODIVIDE : Types::CDIVIDE;
                    else if (strncmp(tag, "modulo", 6) == 0)
                        return (isOpenTag) ? Types::OMODULO : Types::CMODULO;
                    else if (strncmp(tag, "lambda", 6) == 0)
                        return (isOpenTag) ? Types::OLAMBDA : Types::CLAMBDA;
                    break;
            case 7: if (strncmp(tag, "program", 7) == 0)
                        return (isOpenTag) ? Types::OPROGR : Types::CPROGR;
                    break;
            case 8: if (strncmp(tag, "multiply", 8) == 0)
                        return (isOpenTag) ? Types::OMULTIPLY : Types::CMULTIPLY;
                    break;
        }
        return (isOpenTag) ? Types::OID : Types::CID;
    }
}
