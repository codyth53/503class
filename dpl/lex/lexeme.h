/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * lexeme.h
 */

#ifndef LEXEME
#define LEXEME

namespace Lex
{
    class Lexeme
    {
        public:
        char* type;
        char* svalue;
        long int ivalue;
        
        Lexeme(char* argType);
        Lexeme(char* argType, int no);
        Lexeme(char* argType, char* val);
        Lexeme(char* argType, char* val, int no);
        Lexeme(char* argType, long int val);
        Lexeme(char* argType, long int val, int no);
        
        Lexeme* left;
        Lexeme* right;
        
        int line;

        private:
        void clearVals();
    };
}
#endif
