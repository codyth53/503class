/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * lexeme.cpp
 */

#include "lexeme.h"

namespace Lex
{
    Lexeme::Lexeme(char* argType)
    {
    	clearVals();
        type = argType;
        line = -1;
    }
    Lexeme::Lexeme(char* argType, int no)
    {
    	clearVals();
        type = argType;
        line = no;
    }
    Lexeme::Lexeme(char* argType, char* val)
    {
    	clearVals();
        type = argType;
        svalue = val;
        line = -1;
    }
    Lexeme::Lexeme(char* argType, char* val, int no)
    {
    	clearVals();
        type = argType;
        svalue = val;
        line = no;
    }
    Lexeme::Lexeme(char* argType, long int val)
    {
    	clearVals();
        type = argType;
        ivalue = val;
        line = -1;
    }
    Lexeme::Lexeme(char* argType, long int val, int no)
    {
    	clearVals();
        type = argType;
        ivalue = val;
        line = no;
    }
    void Lexeme::clearVals()
    {
    	type = 0;
    	ivalue = 0;
    	svalue = 0;
    	line = -1;
    }
}
