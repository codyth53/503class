/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * scanner.cpp
 */

#include "lexer.h"
#include "lexeme.h"
#include "../shared/types.h"
#include <iostream>

using namespace Shared;
using namespace Lex;
using std::cout;

int main(int argc, char* argv[])
{
	if (argc > 1)
	{
		Lexer::lexerInit(argv[1]);
		Lexeme* lexeme = Lexer::lex();
		while (lexeme->type != Types::EOFT)
		{
			cout << lexeme->type << " ";
			if (lexeme->type == Types::STRING
				|| lexeme->type == Types::ID
				|| lexeme->type == Types::OID
				|| lexeme->type == Types::CID)
				cout << lexeme->svalue;
			else if (lexeme->type == Types::INTEGER)
				cout << lexeme->ivalue;
			cout << "\n";

			lexeme = Lexer::lex();
		}
	}

	return 0;
}
