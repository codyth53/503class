/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * lexer.h
 */

#ifndef LEXER
#define LEXER

#include <fstream>

using std::ifstream;

namespace Lex
{
	class Lexeme;

    class Lexer
    {
        public:
        static void lexerInit(char* filename);
        static Lexeme* lex();
        
        private:
        static void skipWhiteSpace();
        static char getCharacter();
        static void pushback(char ch);
        static Lexeme* getCloseTag();
        static Lexeme* getString();
        static int isNumeric(char ch);
        static Lexeme* getNumber(char ch);
        static int isValidChar(char ch);
        static Lexeme* getTag(char ch);
        static char* getStringUntilChar(char stop, char first);
        static char* recognizeTag(char* tag, int isOpenTag);
        static void skipComment();
        
        static char hold;
        //static FILE* file;
        static std::ifstream file;
        static int lineNo;
    };
}
#endif
