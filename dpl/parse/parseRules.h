/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * parseRules.h
 */

#ifndef RULES
#define RULES

namespace Shared
{
	class Cell;
}
using Shared::Cell;

namespace Parse
{
    Cell* program();
    
    Cell* primary();
    int primaryPending();
    
    Cell* expr();
    int exprPending();
    
    Cell* exprList();
    int exprListPending();
    
    Cell* opExprList();
    int opExprListPending();
    
    Cell* functionCall();
    int functionCallPending();
    
    Cell* block();
    int blockPending();
    
    Cell* statement();
    int statementPending();
    
    Cell* optStatementList();
    int optStatementListPending();
    
    Cell* statementList();
    int statementListPending();
    
    Cell* varDef();
    int varDefPending();
    
    Cell* lambda();
    int lambdaPending();
    
    Cell* wrappedParamList();
    int wrappedParamListPending();
    
    Cell* optParamList();
    int optParamListPending();
    
    Cell* paramList();
    int paramListPending();
    
    Cell* optArgList();
    int optArgListPending();
    
    Cell* argList();
    int argListPending();
    
    Cell* whileLoop();
    int whileLoopPending();
    
    Cell* arrayRef();
    int arrayRefPending();
    
    Cell* array();
    int arrayPending();
    
    Cell* ifStatement();
    int ifStatementPending();
    
    Cell* conditionList();
    int conditionListPending();
    
    Cell* condition();
    int conditionPending();

    Cell* set();
    int setPending();
    
    Cell* openOper();
    int openOperPending();
    
    Cell* closeOper();
    int closeOperPending();
}

#endif
