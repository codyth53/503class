/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * recognizer.cpp
 */

#include "parseRules.h"
#include "parse.h"

int main(int argc, char* argv[])
{
    if (argc > 1)
    {
        Parse::parse(argv[1]);
    }
    
    return 0;
}
