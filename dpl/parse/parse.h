/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * parse.h
 */

#ifndef PARSE
#define PARSE

namespace Shared
{
	class Cell;
}
namespace Lex
{
	class Lexeme;
}
using Shared::Cell;
using Lex::Lexeme;

namespace Parse
{
    Cell* parse(char* filename);
    
    int check(char* type);
    Lexeme* advance();
    Lexeme* match(char* type);
    
    class Parser
    {
        public:
        static Lexeme* pending;
    };
}

#endif
