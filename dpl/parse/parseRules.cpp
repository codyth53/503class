/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * parseRules.cpp
 */

#include "parseRules.h"
#include "parse.h"
#include "../lex/lexeme.h"
#include "../shared/cell.h"
#include "../shared/types.h"
#include <string.h>
#include <stdexcept>

using namespace Shared;
using namespace Lex;

namespace Parse
{
    Cell* program()
    {
        Lexeme* prog = match(Types::OPROGR);
        Cell* states = optStatementList();
        match(Types::CPROGR);
        
        return cons(prog->line, Types::PROGRAM, states, 0);
    }
    
    Cell* primary()
    {
        if (check(Types::ID) || check(Types::STRING))
        {
            Lexeme* l = advance();
            return cons(l->line, Types::PRIMARY, 0, new Cell(l->line, l->type, l->svalue));
        }
        else if (check(Types::INTEGER))
        {
            Lexeme* l = advance();
            return cons(l->line, Types::PRIMARY, 0, new Cell(l->line, l->type, l->ivalue));
        }
        else if (lambdaPending())
        {
        	Cell* thisLambda = lambda();
            return cons(thisLambda->line, Types::PRIMARY, 0, thisLambda);
        }
        else if (check(Types::ONOT))
        {
            Lexeme* notLexeme = match(Types::ONOT);
            Cell* inside = expr();
            match(Types::CNOT);
            return cons(notLexeme->line, Types::PRIMARY, new Cell(notLexeme->line, Types::ONOT), inside);
        }
        else if (arrayRefPending())
        {
        	Cell* thisRef = arrayRef();
            return cons(thisRef->line, Types::PRIMARY, 0, thisRef);
        }
        else if (functionCallPending())
        {
        	Cell* thisFunc = functionCall();
            return cons(thisFunc->line, Types::PRIMARY, 0, thisFunc);
        }
        else if (arrayPending())
        {
        	Cell* thisArr = array();
            return cons(thisArr->line, Types::PRIMARY, 0, thisArr);
        }
        else
            throw std::runtime_error("Primary called, but no primary options found");
    }
    int primaryPending()
    {
        return (check(Types::ID)
                || check(Types::STRING)
                || check(Types::INTEGER)
                || lambdaPending()
                || check(Types::ONOT)
                || arrayRefPending()
                || functionCallPending()
                || arrayPending());
    }
    
    Cell* expr()
    {
        if (openOperPending())
        {
            Cell* oper = openOper();
            Cell* list = exprList();
            closeOper();
            return cons(oper->line, Types::EXPR, oper, list);
        }
        else
        {
        	Cell* prim = primary();
            return cons(prim->line, Types::EXPR, prim, 0);
        }
    }
    int exprPending()
    {
        return (primaryPending()
                || openOperPending());
    }
    
    Cell* exprList()
    {
        Cell* exp = expr();
        if (exprListPending())
            return cons(exp->line, Types::EXPRLIST, exp, exprList());
        else
            return cons(exp->line, Types::EXPRLIST, exp, 0);
    }
    int exprListPending()
    {
        return exprPending();
    }
    
    Cell* optExprList()
    {
        if (exprListPending())
            return exprList();
        else
            return 0;
    }
    int optExprListPending()
    {
        return 1;
    }
    
    Cell* functionCall()
    {
        if (check(Types::OID))
        {
            Lexeme* id = match(Types::OID);
            Cell* args = optArgList();
            Lexeme* cid = match(Types::CID);
            if (strcmp(id->svalue, cid->svalue) != 0)
                throw std::runtime_error("Mismatched id tags");
            return cons(id->line, Types::FUNCTIONCALL, new Cell(id->line, id->type, id->svalue), args);
        }
        else if (check(Types::OCALL))
        {
            match(Types::OCALL);
            Cell* prim = primary();
            Cell* args = optArgList();
            match(Types::CCALL);
            return cons(prim->line, Types::FUNCTIONCALL, prim, args);
        }
        else
            throw 20;
    }
    int functionCallPending()
    {
        return (check(Types::OID)
                || check(Types::OCALL));
    }
    
    Cell* block()
    {
        Lexeme* block = match(Types::OBLOCK);
        Cell* states = optStatementList();
        match(Types::CBLOCK);
        return cons(block->line, Types::BLOCK, 0, states);
    }
    int blockPending()
    {
        return check(Types::OBLOCK);
    }
    
    Cell* statement()
    {
        if (exprPending())
            return expr();
        else if (varDefPending())
            return varDef();
        else if (whileLoopPending())
            return whileLoop();
        else if (ifStatementPending())
            return ifStatement();
        else if (check(Types::ORETURN))
        {
            Lexeme* ret = match(Types::ORETURN);
            Cell* exp = 0;
            if (exprPending())
                exp = expr();
            match(Types::CRETURN);
            return cons(ret->line, Types::ORETURN, 0, exp);
        }
        else if (blockPending())
            return block();
        else if (setPending())
            return set();
        else
            throw std::runtime_error("Statement called, but no statement starts found");
    }
    int statementPending()
    {
        return (exprPending()
                || varDefPending()
                || whileLoopPending()
                || ifStatementPending()
                || check(Types::ORETURN)
                || blockPending()
                || setPending());
    }
    
    Cell* optStatementList()
    {
        if (statementListPending())
            return statementList();
        else
            return 0;
    }
    int optStatementListPending()
    {
        return 1;
    }
    
    Cell* statementList()
    {
        Cell* state = statement();
        if (statementListPending())
            return cons(state->line, Types::STATEMENTLIST, state, statementList());
        else
            return cons(state->line, Types::STATEMENTLIST, state, 0);
    }
    int statementListPending()
    {
        return statementPending();
    }
    
    Cell* varDef()
    {
        Lexeme* def = match(Types::ODEF);
        if (check(Types::ID))
        {
            Lexeme* id = match(Types::ID);
            Cell* exp = expr();
            match(Types::CDEF);
            return cons(def->line, Types::VARDEF, new Cell(id->line, id->type, id->svalue), exp);
        }
        else if (check(Types::OID))
        {
            Lexeme* id = match(Types::OID);
            Cell* params = optParamList();
            Lexeme* cid = match(Types::CID);
            if (strncmp(id->svalue, cid->svalue, strlen(id->svalue))!= 0)
                throw std::runtime_error("Open tag and Close tag doesn't match in varDef");
            Cell* statements = statementList();
            match(Types::CDEF);
            return cons(def->line, Types::VARDEF, cons(id->line, Types::JOIN, new Cell(id->line, id->type, id->svalue), params), statements);
        }
        else
            throw std::runtime_error("varDef called, but ID or OID not found");
    }
    int varDefPending()
    {
        return check(Types::ODEF);
    }
    
    Cell* lambda()
    {
        Lexeme* lamb = match(Types::OLAMBDA);
        Cell* params = wrappedParamList();
        Cell* statements = statementList();
        match(Types::CLAMBDA);
        return cons(lamb->line, Types::LAMBDA, params, statements);
    }
    int lambdaPending()
    {
        return check(Types::OLAMBDA);
    }
    
    Cell* wrappedParamList()
    {
        match(Types::OPARAM);
        Cell* params = optParamList();
        match(Types::CPARAM);
        return params;
    }
    int wrappedParamListPending()
    {
        return check(Types::OPARAM);
    }
    
    Cell* optParamList()
    {
        if (paramListPending())
            return paramList();
        else
            return 0;
    }
    int optParamListPending()
    {
        return 1;
    }
    
    Cell* paramList()
    {
        bool isDelayed = false;
        Lexeme* delay;
        if (check(Types::ODELAY))
        {
            isDelayed = true;
            delay = match(Types::ODELAY);
        }
        Lexeme* id = match(Types::ID);
        if (isDelayed)
            match(Types::CDELAY);

        Cell* cid;
        if (isDelayed)
            cid = new Cell(delay->line, Types::ODELAY, new Cell(id->line, id->type, id->svalue), 0);
        else
            cid = new Cell(id->line, id->type, id->svalue);
        if (paramListPending())
            return cons(id->line, Types::PARAMLIST, cid, paramList());
        else
            return cons(id->line, Types::PARAMLIST, cid, 0);
    }
    int paramListPending()
    {
        return (check(Types::ID)
                || check(Types::ODELAY));
    }
    
    Cell* optArgList()
    {
        if (argListPending())
            return argList();
        else
            return 0;
    }
    int optArgListPending()
    {
        return 1;
    }
    
    Cell* argList()
    {
        Cell* exp = expr();
        if (argListPending())
            return cons(exp->line, Types::ARGLIST, exp, argList());
        else
            return cons(exp->line, Types::ARGLIST, exp, 0);
    }
    int argListPending()
    {
        return exprPending();
    }
    
    Cell* whileLoop()
    {
        Lexeme* loop = match(Types::OLOOP);
        match(Types::OCOND);
        Cell* cond = expr();
        match(Types::CCOND);
        Cell* statements = statementList();
        match(Types::CLOOP);
        return cons(loop->line, Types::WHILELOOP, cond, statements);
    }
    int whileLoopPending()
    {
        return check(Types::OLOOP);
    }
    
    Cell* arrayRef()
    {
        Lexeme* get = match(Types::OGET);
        Cell* prim = primary();
        Cell* index = expr();
        match(Types::CGET);
        return cons(get->line, Types::ARRAYREF, prim, index);
    }
    int arrayRefPending()
    {
        return check(Types::OGET);
    }
    
    Cell* array()
    {
        Lexeme* arr = match(Types::OARRAY);
        Cell* exprs = optExprList();
        match(Types::CARRAY);
        return cons(arr->line, Types::ARRAY, exprs, 0);
    }
    int arrayPending()
    {
        return check(Types::OARRAY);
    }
    
    Cell* ifStatement()
    {
        Lexeme* ifstate = match(Types::OIF);
        Cell* conditions = conditionList();
        match(Types::CIF);
        return cons(ifstate->line, Types::IFSTATEMENT, conditions, 0);
    }
    int ifStatementPending()
    {
        return check(Types::OIF);
    }
    
    Cell* conditionList()
    {
        Cell* cond = condition();
        if (conditionListPending())
            return cons(cond->line, Types::CONDITIONLIST, cond, conditionList());
        else
            return cons(cond->line, Types::CONDITIONLIST, cond, 0);
    }
    int conditionListPending()
    {
        return conditionPending();
    }
    
    Cell* condition()
    {
        Lexeme* cond = match(Types::OCOND);
        Cell* exp;
        if (check(Types::OELSE))
        {
			Lexeme* l = match(Types::OELSE);
			exp = new Cell(l->line, l->type);
			match(Types::CELSE);
		}
		else
			exp = expr();
        Cell* states = statementList();
        match(Types::CCOND);
        return cons(cond->line, Types::CONDITION, exp, states);
    }
    int conditionPending()
    {
        return check(Types::OCOND);
    }

    Cell* set()
    {
        Lexeme* set = match(Types::OSET);
        Cell* id = expr();
        Cell* newVal = expr();
        match(Types::CSET);
        return cons(set->line, Types::SET, id, newVal);
    }
    int setPending()
    {
        return check(Types::OSET);
    }
    
    Cell* openOper()
    {
        if (openOperPending())
        {
            Lexeme* oper = advance();
            return cons(oper->line, oper->type, 0, 0);
        }
        else
            throw std::runtime_error("openOper called, but openOper not pending");
    }
    int openOperPending()
    {
        return (check(Types::OPLUS)
                || check(Types::OMINUS)
                || check(Types::OMULTIPLY)
                || check(Types::ODIVIDE)
                || check(Types::OMODULO)
                || check(Types::OPTREQ)
                || check(Types::OEQ)
                || check(Types::OLT)
                || check(Types::OGT)
                || check(Types::OLE)
                || check(Types::OGE)
                || check(Types::ONE)
                || check(Types::OAND)
                || check(Types::OOR));
    }
    
    Cell* closeOper()
    {
        if (closeOperPending())
        {
            Lexeme* oper = advance();
            return cons(oper->line, oper->type, 0, 0);
        }
        else
            throw std::runtime_error("closeOper called, but closeOper not pending");
    }
    int closeOperPending()
    {
        return (check(Types::CPLUS)
                || check(Types::CMINUS)
                || check(Types::CMULTIPLY)
                || check(Types::CDIVIDE)
                || check(Types::CMODULO)
                || check(Types::CPTREQ)
                || check(Types::CEQ)
                || check(Types::CLT)
                || check(Types::CGT)
                || check(Types::CLE)
                || check(Types::CGE)
                || check(Types::CNE)
                || check(Types::CAND)
                || check(Types::COR));
    }
}
