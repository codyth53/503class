/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * parse.cpp
 */

#include "parse.h"
#include "parseRules.h"
#include "../shared/cell.h"
#include "../shared/types.h"
#include "../lex/lexeme.h"
#include "../lex/lexer.h"
#include <stdio.h>
#include <stdexcept>
#include <iostream>

using namespace Lex;
using namespace Shared;
using std::cout;

namespace Parse
{
    Lexeme* Parser::pending = new Lexeme(Types::UNKNOWN);
    
    Cell* parse(char* fileName)
    {
        try{
            Lexer::lexerInit(fileName);
        
            Parser::pending = Lexer::lex();
            Cell* prog = program();
            match(Types::EOFT);
            
            return prog;
        }
        catch (int e) { cout << "Error no. " << e << "\n"; }
        catch (char e[]) { cout << "Error no. " << e << "\n"; }
        catch (char const* e) { cout << "Error: " << e << "\n"; }
        catch (std::runtime_error &e)
        {
            cout << "Error: " << e.what() << "\n";
            cout << "Last pending: " << Parser::pending->type << " line " << Parser::pending->line << "\n";
        }
        return 0;
    }
    
    int check(char* type)
    {
        return (type == Parser::pending->type);
    }
    
    Lexeme* advance()
    {
        Lexeme* old = Parser::pending;
        Parser::pending = Lexer::lex();
        return old;
    }
    
    Lexeme* match(char* type)
    {
        if (check(type))
            return advance();
        else
        {
            char message[40];
            sprintf(message, "Expected %s, received %s", type, Parser::pending->type);
            throw std::runtime_error(message);
        }
    }
    
    /*Cell* cons(char* type, Cell* left, Cell* right)
    {
        //Lexeme* l = new Lexeme(type);
        //l->left = left;
        //l->right = right;
        return new Cell(type, left, right);
    }*/
}
