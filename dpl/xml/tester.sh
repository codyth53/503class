#!/bin/bash


for f in ./xml/*.xml
do
    if [ "$f" == "./xml/sticksgame.xml" ] ; then
        continue;
    fi
    if [ "$f" == "./xml/chomp.xml" ] ; then
        continue;
    fi
    echo "Testing $f file..."
    ./xmlEval $f
    echo "Done with $f file..."
    echo
done
