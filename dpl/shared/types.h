/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * types.h
 */

#ifndef TYPES
#define TYPES

namespace Shared
{
    class Types
    {
        public:
        //Lexemes
        static char ID[];
        static char OID[];
        static char CID[];
        static char STRING[];
        static char INTEGER[];
        static char ONOT[];
        static char CNOT[];
        static char OCALL[];
        static char CCALL[];
        static char OBLOCK[];
        static char CBLOCK[];
        static char ORETURN[];
        static char CRETURN[];
        static char OPROGR[];
        static char CPROGR[];
        static char ODEF[];
        static char CDEF[];
        static char OLAMBDA[];
        static char CLAMBDA[];
        static char OPARAM[];
        static char CPARAM[];
        static char OLOOP[];
        static char CLOOP[];
        static char OCOND[];
        static char CCOND[];
        static char OGET[];
        static char CGET[];
        static char OARRAY[];
        static char CARRAY[];
        static char OIF[];
        static char CIF[];
        static char OPLUS[];
        static char CPLUS[];
        static char OMINUS[];
        static char CMINUS[];
        static char OMULTIPLY[];
        static char CMULTIPLY[];
        static char ODIVIDE[];
        static char CDIVIDE[];
        static char OMODULO[];
        static char CMODULO[];
        static char OPTREQ[];
        static char CPTREQ[];
        static char OEQ[];
        static char CEQ[];
        static char OLT[];
        static char CLT[];
        static char OGT[];
        static char CGT[];
        static char OLE[];
        static char CLE[];
        static char OGE[];
        static char CGE[];
        static char ONE[];
        static char CNE[];
        static char OAND[];
        static char CAND[];
        static char OOR[];
        static char COR[];
        static char OSET[];
        static char CSET[];
        static char ODELAY[];
        static char CDELAY[];
        static char OELSE[];
        static char CELSE[];
        static char UNKNOWN[];
        static char EOFT[];
        
        //Parse trees
        static char PROGRAM[];
        static char PRIMARY[];
        static char EXPR[];
        static char EXPRLIST[];
        static char OPTEXPRLIST[];
        static char FUNCTIONCALL[];
        static char BLOCK[];
        static char STATEMENT[];
        static char OPTSTATEMENTLIST[];
        static char STATEMENTLIST[];
        static char VARDEF[];
        static char LAMBDA[];
        static char WRAPPEDPARAMLIST[];
        static char OPTPARAMLIST[];
        static char PARAMLIST[];
        static char OPTARGLIST[];
        static char ARGLIST[];
        static char WHILELOOP[];
        static char ARRAYREF[];
        static char ARRAY[];
        static char IFSTATEMENT[];
        static char CONDITIONLIST[];
        static char CONDITION[];
        static char OPERATOR[];
        static char SET[];
        static char THUNK[];
        
        static char NOT[];
        
        static char JOIN[];
        static char ENV[];

        static char CLOSURE[];
        static char BUILT[];
    };
}

#endif
