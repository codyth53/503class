/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * cell.h
 */

#ifndef CELL
#define CELL

#include <vector>

using std::vector;

namespace Shared
{
    class Cell;
    typedef Cell* (*fptr)(Cell*, Cell*);
    
	class Cell;

	Cell* cons(char* type, Cell* left, Cell* right);
	Cell* cons(int lineNum, char* type, Cell* left, Cell* right);
	
    class Cell
    {
        public:
        char* type;
        char* svalue;
        long int ivalue;
        Cell* cvalue;
        fptr fvalue;
        vector<Cell*>* avalue;
        int line;

        
        Cell(const Cell& old);
        Cell(char* argType);
        Cell(int lineNum, char* argType);
        Cell(char* argType, char* val);
        Cell(int lineNum, char* argType, char* val);
        Cell(int lineNum, char* argType, long int val);
        Cell(int lineNum, char* argType, int val);
        Cell(int lineNum, char* argType, vector<Cell*>* val);
        Cell(char* argType, Cell* val);
        Cell(int lineNum, char* argType, Cell* val);
        Cell(char* argType, fptr val);
        Cell(int lineNum, char* argType, Cell* left, Cell* right);

        void morph(Cell* old);
        
        Cell* left;
        Cell* right;
        
        Cell* car();
        Cell* cdr();
        Cell* setcar(Cell* l);
        Cell* setcdr(Cell* r);
        
        private:
        void clearVals();
    };
}
#endif
