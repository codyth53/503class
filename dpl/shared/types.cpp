/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * types.cpp
 */

#include "types.h"

namespace Shared
{
    //Lexemes
    char Types::ID[] = "ID";
    char Types::OID[] = "OID";
    char Types::CID[] = "CID";
    char Types::STRING[] = "STRING";
    char Types::INTEGER[] = "INTEGER";
    char Types::ONOT[] = "ONOT";
    char Types::CNOT[] = "CNOT";
    char Types::OCALL[] = "OCALL";
    char Types::CCALL[] = "CCALL";
    char Types::OBLOCK[] = "OBLOCK";
    char Types::CBLOCK[] = "CBLOCK";
    char Types::ORETURN[] = "ORETURN";
    char Types::CRETURN[] = "CRETURN";
    char Types::OPROGR[] = "OPROGR";
    char Types::CPROGR[] = "CPROGR";
    char Types::ODEF[] = "ODEF";
    char Types::CDEF[] = "CDEF";
    char Types::OLAMBDA[] = "OLAMBDA";
    char Types::CLAMBDA[] = "CLAMBDA";
    char Types::OPARAM[] = "OPARAM";
    char Types::CPARAM[] = "CPARAM";
    char Types::OLOOP[] = "OLOOP";
    char Types::CLOOP[] = "CLOOP";
    char Types::OCOND[] = "OCOND";
    char Types::CCOND[] = "CCOND";
    char Types::OGET[] = "OGET";
    char Types::CGET[] = "CGET";
    char Types::OARRAY[] = "OARRAY";
    char Types::CARRAY[] = "CARRAY";
    char Types::OIF[] = "OIF";
    char Types::CIF[] = "CIF";
    char Types::OPLUS[] = "OPLUS";
    char Types::CPLUS[] = "CPLUS";
    char Types::OMINUS[] = "OMINUS";
    char Types::CMINUS[] = "CMINUS";
    char Types::OMULTIPLY[] = "OMULTIPLY";
    char Types::CMULTIPLY[] = "CMULTIPLY";
    char Types::ODIVIDE[] = "ODIVIDE";
    char Types::CDIVIDE[] = "CDIVIDE";
    char Types::OMODULO[] = "OMODULO";
    char Types::CMODULO[] = "CMODULO";
    char Types::OPTREQ[] = "OPTREQ";
    char Types::CPTREQ[] = "CPTREQ";
    char Types::OEQ[] = "OEQ";
    char Types::CEQ[] = "CEQ";
    char Types::OLT[] = "OLT";
    char Types::CLT[] = "CLT";
    char Types::OGT[] = "OGT";
    char Types::CGT[] = "CGT";
    char Types::OLE[] = "OLE";
    char Types::CLE[] = "CLE";
    char Types::OGE[] = "OGE";
    char Types::CGE[] = "CGE";
    char Types::ONE[] = "ONE";
    char Types::CNE[] = "CNE";
    char Types::OAND[] = "OAND";
    char Types::CAND[] = "CAND";
    char Types::OOR[] = "OOR";
    char Types::COR[] = "COR";
    char Types::OSET[] = "OSET";
    char Types::CSET[] = "CSET";
    char Types::ODELAY[] = "ODELAY";
    char Types::CDELAY[] = "CDELAY";
    char Types::OELSE[] = "OELSE";
    char Types::CELSE[] = "CELSE";
    char Types::UNKNOWN[] = "UNKNOWN";
    char Types::EOFT[] = "EOF";
    
    //Parse Types
    char Types::PROGRAM[] = "PROGRAM";
    char Types::PRIMARY[] = "PRIMARY";
    char Types::EXPR[] = "EXPR";
    char Types::EXPRLIST[] = "EXPRLIST";
    char Types::OPTEXPRLIST[] = "OPTEXPRLIST";
    char Types::FUNCTIONCALL[] = "FUNCTIONCALL";
    char Types::BLOCK[] = "BLOCK";
    char Types::STATEMENT[] = "STATEMENT";
    char Types::OPTSTATEMENTLIST[] = "OPTSTATEMENTLIST";
    char Types::STATEMENTLIST[] = "STATEMENTLIST";
    char Types::VARDEF[] = "VARDEF";
    char Types::LAMBDA[] = "LAMBDA";
    char Types::WRAPPEDPARAMLIST[] = "WRAPPEDPARAMLIST";
    char Types::OPTPARAMLIST[] = "OPTPARAMLIST";
    char Types::PARAMLIST[] = "PARAMLIST";
    char Types::OPTARGLIST[] = "OPTARGLIST";
    char Types::ARGLIST[] = "ARGLIST";
    char Types::WHILELOOP[] = "WHILELOOP";
    char Types::ARRAYREF[] = "ARRAYREF";
    char Types::ARRAY[] = "ARRAY";
    char Types::IFSTATEMENT[] = "IFSTATEMENT";
    char Types::CONDITIONLIST[] = "CONDITIONLIST";
    char Types::CONDITION[] = "CONDITION";
    char Types::OPERATOR[] = "OPERATOR";
    char Types::SET[] = "SET";
    char Types::THUNK[] = "THUNK";
    
    char Types::NOT[] = "NOT";
    
    char Types::JOIN[] = "JOIN";
    char Types::ENV[] = "ENV";

    char Types::CLOSURE[] = "CLOSURE";
    char Types::BUILT[] = "BUILT";
}
