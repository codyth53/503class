/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * cell.cpp
 */

#include "cell.h"

namespace Shared
{
    Cell* cons(char* type, Cell* left, Cell* right)
    {
        return cons(-1, type, left, right);
    }
    Cell* cons(int lineNum, char* type, Cell* left, Cell* right)
    {
        return new Cell(lineNum, type, left, right);
    }

    Cell::Cell(const Cell& old)
    {
    	type = old.type;
    	svalue = old.svalue;
    	ivalue = old.ivalue;
    	avalue = old.avalue;
    	fvalue = old.fvalue;
    	cvalue = old.cvalue;
    	left = old.left;
    	right = old.right;
    	line = old.line;
    }
    Cell::Cell(char* argType)
    {
    	clearVals();
        type = argType;
    }
    Cell::Cell(int lineNum, char* argType)
    {
    	clearVals();
        type = argType;
        line = lineNum;
    }
    Cell::Cell(char* argType, char* val)
    {
        clearVals();
        type = argType;
        svalue = val;
    }
    Cell::Cell(int lineNum, char* argType, char* val)
    {
        clearVals();
        type = argType;
        svalue = val;
        line = lineNum;
    }
    Cell::Cell(int lineNum, char* argType, long int val)
    {
        clearVals();
        type = argType;
        ivalue = val;
        line = lineNum;
    }
    Cell::Cell(int lineNum, char* argType, int val)
    {
    	clearVals();
    	type = argType;
    	ivalue = (long int)val;
        line = lineNum;
    }
    Cell::Cell(int lineNum, char* argType, vector<Cell*>* val)
    {
    	clearVals();
    	type = argType;
    	avalue = val;
        line = lineNum;
    }
    Cell::Cell(char* argType, Cell* val)
    {
        clearVals();
        type = argType;
        cvalue = val;
    }
    Cell::Cell(int lineNum, char* argType, Cell* val)
    {
        clearVals();
        type = argType;
        cvalue = val;
        line = lineNum;
    }
    Cell::Cell(char* argType, fptr val)
    {
        clearVals();
        type = argType;
        fvalue = val;
    }
    Cell::Cell(int lineNum, char* argType, Cell* l, Cell* r)
    {
        clearVals();
        type = argType;
        left = l;
        right = r;
        line = lineNum;
    }
    
    void Cell::morph(Cell* old)
    {
    	type = old->type;
		svalue = old->svalue;
		ivalue = old->ivalue;
		cvalue = old->cvalue;
		fvalue = old->fvalue;
		avalue = old->avalue;
		line = old->line;
    }

    Cell* Cell::car()
    {
        return left;
    }
    Cell* Cell::cdr()
    {
        return right;
    }
    Cell* Cell::setcar(Cell* l)
    {
        Cell* old = left;
        left = l;
        return old;
    }
    Cell* Cell::setcdr(Cell* r)
    {
        Cell* old = right;
        right = r;
        return old;
    }
    
    void Cell::clearVals()
    {
        svalue = 0;
        cvalue = 0;
        ivalue = -1;
        fvalue = 0;
        avalue = 0;
        line = -1;
    }
}
