The one-bit full adder is located in the file xml/adder.xml.
To include it in a program, use <include><"adder.xml"/></include>.

An example use of adder.xml is located in xml/addertester.xml

Set <andgatedelay/>, <orgatedelay/>, and <inverterdelay/> to change delays.

Wires can be created using <makewire></makewire>.

Use <setsignal>{wire}{value}</setsignal> to set the signal of a wire.
Use <getsignal>{wire}</getsignal> to get the signal of a wire.

The full one-bit adder can be created using:
<add1>{in1}{in2}{cin}{s}{cout}</add1>

Use <propagate></propagate> to begin the propagation.
