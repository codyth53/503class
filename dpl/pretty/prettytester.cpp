#include "prettyprint.h"
#include "../shared/cell.h"
#include "../parse/parse.h"
#include <iostream>

using namespace Shared;
using namespace Parse;
using namespace Pretty;
using std::cout;

int main(int argc, char* argv[])
{
    if (argc > 1)
    {
        Cell* tree = parse(argv[1]);
        prettify(cout, tree);
        cout << "\n";
    }
    
    return 0;
}
