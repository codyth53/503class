#include "prettyprint.h"
#include "../shared/types.h"
#include "../shared/cell.h"
#include <stdlib.h>

using namespace Shared;
using std::cout;

namespace Pretty
{
    void prettify(ostream& stm, Cell* tree)
    {
        prettify(stm, tree, 0);
    }
    void prettify(ostream& stm, Cell* tree, int level)
    {
        char* buff = (char*)malloc(sizeof(char)*2*level+2);
        buff[0] = '\n';
        int i;
        for (i=0; i<level; i++)
        {
            buff[i*2+1] = buff[i*2+2] = ' ';
        }
        buff[level*2+1] = 0;
            
        if (tree->type == Types::PROGRAM)
        {
            stm << buff << "<program>";
            prettify(stm, tree->car(), level+1);
            stm << buff << "</program>";
        }
        else if (tree->type == Types::PRIMARY)
        {
            if (tree->car() != 0 && tree->car()->type == Types::ONOT)
                stm << buff << "<not>";
            
            Cell* primary = tree->cdr();
            if (primary->type == Types::ID)
            {
                stm << buff << "<" << primary->svalue << "/>";
            }
            else if (primary->type == Types::STRING)
            {
                stm << buff << "<\"" << primary->svalue << "\"/>";
            }
            else if (primary->type == Types::INTEGER)
            {
                stm << buff << "<" << primary->ivalue << "/>";
            }
            else
            {
                prettify(stm, tree->cdr(), level);
            }
            
            if (tree->car() != 0 && tree->car()->type == Types::ONOT)
                stm << buff << "</not>";
        }
        else if (tree->type == Types::EXPR)
        {
            Cell* child = tree->car();
            
            if (child->type == Types::PRIMARY)
            {
                prettify(stm, child, level);
            }
            else if (child->type == Types::OPLUS)
            {
                stm << buff << "<plus>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</plus>";
            }
            else if (child->type == Types::OMINUS)
            {
                stm << buff << "<minus>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</minus>";
            }
            else if (child->type == Types::OMULTIPLY)
            {
                stm << buff << "<multiply>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</multiply>";
            }
            else if (child->type == Types::ODIVIDE)
            {
                stm << buff << "<divide>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</divide>";
            }
            else if (child->type == Types::OMODULO)
            {
                stm << buff << "<modulo>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</modulo>";
            }
            else if (child->type == Types::OEQ)
            {
                stm << buff << "<eq>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</eq>";
            }
            else if (child->type == Types::OLT)
            {
                stm << buff << "<lt>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</lt>";
            }
            else if (child->type == Types::OGT)
            {
                stm << buff << "<gt>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</gt>";
            }
            else if (child->type == Types::OLE)
            {
                stm << buff << "<le>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</le>";
            }
            else if (child->type == Types::OGE)
            {
                stm << buff << "<ge>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</ge>";
            }
            else if (child->type == Types::ONE)
            {
                stm << buff << "<ne>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</ne>";
            }
            else if (child->type == Types::OAND)
            {
                stm << buff << "<and>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</and>";
            }
            else if (child->type == Types::OOR)
            {
                stm << buff << "<or>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</or>";
            }
            else if (child->type == Types::ONOT)
            {
                stm << buff << "<not>";
                prettify(stm, tree->cdr(), level+1);
                stm << buff << "</not>";
            }
            else
            {
                throw "Expected operator but didn't find one";
            }
        }
        else if (tree->type == Types::EXPRLIST)
        {
            prettify(stm, tree->car(), level);
            if (tree->cdr() != 0)
                prettify(stm, tree->cdr(), level);
        }
        else if (tree->type == Types::FUNCTIONCALL)
        {
            Cell* left = tree->car();
            if (left->type == Types::OID)
            {
                stm << buff << "<" << left->svalue << ">";
                if (tree->cdr() != 0)
                    prettify(stm, tree->cdr(), level+1);
                stm << buff << "</" << left->svalue << ">";
            }
            else //<call></call>
            {
                stm << buff << "<call>";
                prettify(stm, left, level+1);
                if (tree->cdr() != 0)
                    prettify(stm, tree->cdr(), level+1);
                stm << buff << "</call>";
            }
        }
        else if (tree->type == Types::BLOCK)
        {
            stm << buff << "<block>";
            if (tree->cdr() != 0)
                prettify(stm, tree->cdr(), level+1);
            stm << buff << "</block>";
        }
        else if (tree->type == Types::ORETURN)
        {
            stm << buff << "<return>";
            if (tree->cdr() != 0)
                prettify(stm, tree->cdr(), level+1);
            stm << buff << "</return>";
        }
        else if (tree->type == Types::STATEMENTLIST)
        {
            prettify(stm, tree->car(), level);
            if (tree->cdr() != 0)
                prettify(stm, tree->cdr(), level);
        }
        else if (tree->type == Types::VARDEF)
        {
            stm << buff << "<def>";
            Cell* left = tree->car();
            if (left->type == Types::ID)
            {
                stm << buff << "  <" << left->svalue << "/>";
                prettify(stm, tree->cdr(), level+1);
            }
            else //function def
            {
                Cell* id = left->car();
                stm << buff << "  <" << id->svalue << ">";
                if (left->cdr() != 0)
                    prettify(stm, left->cdr(), level+2);
                stm << buff << "  </" << id->svalue << ">";
                prettify(stm, tree->cdr(), level+1);
            }
            stm << buff << "</def>";
        }
        else if (tree->type == Types::LAMBDA)
        {
            stm << buff << "<lambda>" << buff << "  <param>";
            if (tree->car() != 0)
                prettify(stm, tree->car(), level+2);
            stm << buff << "  </param>";
            prettify(stm, tree->cdr(), level+1);
            stm << buff << "</lambda>";
        }
        else if (tree->type == Types::PARAMLIST)
        {
            if (tree->car()->type == Types::ODELAY)
            {
                stm << buff << "<delay>";
                stm << buff << "  <" << tree->car()->car()->svalue << "/>";
                stm << buff << "</delay>";
            }
            else
                stm << buff << "<" << tree->car()->svalue << "/>";
            if (tree->cdr() != 0)
                prettify(stm, tree->cdr(), level);
        }
        else if (tree->type == Types::ARGLIST)
        {
            prettify(stm, tree->car(), level);
            if (tree->cdr() != 0)
                prettify(stm, tree->cdr(), level);
        }
        else if (tree->type == Types::WHILELOOP)
        {
            stm << buff << "<loop>" << buff << "  <cond>";
            prettify(stm, tree->car(), level+2);
            stm << buff << "  </cond>";
            prettify(stm, tree->cdr(), level+1);
            stm << buff << "</loop>";
        }
        else if (tree->type == Types::ARRAYREF)
        {
            stm << buff << "<get>";
            stm << buff << "  <" << tree->car()->svalue << "/>";
            prettify(stm, tree->cdr(), level+1);
            stm << buff << "</get>";
        }
        else if (tree->type == Types::ARRAY)
        {
            stm << buff << "<array>";
            if (tree->car() != 0)
                prettify(stm, tree->car(), level+1);
            stm << buff << "</array>";
        }
        else if (tree->type == Types::IFSTATEMENT)
        {
            stm << buff << "<if>";
            prettify(stm, tree->car(), level+1);
            stm << buff << "</if>";
        }
        else if (tree->type == Types::CONDITIONLIST)
        {
            prettify(stm, tree->car(), level);
            if (tree->cdr() != 0)
                prettify(stm, tree->cdr(), level);
        }
        else if (tree->type == Types::CONDITION)
        {
            stm << buff << "<cond>";
            prettify(stm, tree->car(), level+1);
            prettify(stm, tree->cdr(), level+1);
            stm << buff << "</cond>";
        }
        else if (tree->type == Types::SET)
        {
            stm << buff << "<set>";
            prettify(stm, tree->car(), level+1);
            prettify(stm, tree->cdr(), level+1);
            stm << buff << "</set>";
        }
        else if (tree->type == Types::OELSE)
        {
			stm << buff << "<else>";
			stm << buff << "</else>";
		}
        else
        {
            throw "Expected recognized parse tree but didn't find one.";
        }
        
        free(buff);
    }
}
