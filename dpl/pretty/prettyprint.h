

#ifndef PRETTY
#define PRETTY

#include <iostream>

namespace Shared
{
	class Cell;
}
using Shared::Cell;
using std::ostream;

namespace Pretty
{
    void prettify(ostream& stm, Cell* tree);
    void prettify(ostream& stm, Cell* tree, int level);
}

#endif
