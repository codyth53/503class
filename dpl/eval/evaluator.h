/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * evaluator.h
 */

#ifndef EVALUATOR
#define EVALUATOR

namespace Shared
{
	class Cell;
}
using Shared::Cell;

namespace Evaluator
{
    typedef Cell* (*fptr)(Cell*, Cell*);
    
    Cell* eval(Cell* tree, Cell* env);
    
    Cell* evalProgram(Cell* tree, Cell* env);
    Cell* evalPrimary(Cell* tree, Cell* env);
    Cell* evalStatementList(Cell* tree, Cell* env);
    Cell* evalDef(Cell* tree, Cell* env);
    Cell* evalSet(Cell* tree, Cell* env);
    Cell* evalCall(Cell* tree, Cell* env);
    Cell* evalArgs(Cell* args, Cell* params, Cell* env);
    Cell* evalLambda(Cell* tree, Cell* env);
    Cell* evalIf(Cell* tree, Cell* env);
    Cell* evalWhileLoop(Cell* tree, Cell* env);
    Cell* evalBlock(Cell* tree, Cell* env);
    Cell* evalArray(Cell* tree, Cell* env);
    Cell* evalArrayRef(Cell* tree, Cell* env);
    Cell* evalReturn(Cell* tree, Cell* env);
    Cell* evalThunk(Cell* tree, Cell* env);
    
    Cell* evalPlus(Cell* tree, Cell* env);
    Cell* evalMinus(Cell* tree, Cell* env);
    Cell* evalMultiply(Cell* tree, Cell* env);
    Cell* evalDivide(Cell* tree, Cell* env);
    Cell* evalModulo(Cell* tree, Cell* env);
    Cell* evalPtrEq(Cell* tree, Cell* env);
    Cell* evalEq(Cell* tree, Cell* env);
    Cell* evalLt(Cell* tree, Cell* env);
    Cell* evalGt(Cell* tree, Cell* env);
    Cell* evalLe(Cell* tree, Cell* env);
    Cell* evalGe(Cell* tree, Cell* env);
    Cell* evalNe(Cell* tree, Cell* env);
    Cell* evalAnd(Cell* tree, Cell* env);
    Cell* evalOr(Cell* tree, Cell* env);
}

#endif
