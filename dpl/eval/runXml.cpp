/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * runXml.cpp
 */

#include "evaluator.h"
#include "evalHelper.h"
#include "builtin.h"
#include "../shared/cell.h"
#include "../parse/parse.h"
#include "../environ/environ.h"
#include <iostream>
#include <stdexcept>

using namespace Parse;
using namespace Environment;
using namespace Evaluator;
using std::cout;

int main(int argc, char* argv[])
{
    if (argc > 1)
    {
    	try {
    		Cell* tree = parse(argv[1]);
    		Cell* env = buildGlobalBuiltins();
    		eval(tree, env);
    	}
    	catch (const char e)
    	{
    		cout << "Error occurred\n";
    		cout << e << "\n";
    	}
    	catch (const std::exception &e)
    	{
    		cout << "Error occurred\n";
    		cout << e.what() << "\n";
    	}
    }
    
    return 0;
}
