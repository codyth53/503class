/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * builtin.cpp
 */

#include "builtin.h"

#include "../shared/types.h"
#include "../shared/cell.h"
#include "../environ/environ.h"
#include "../parse/parse.h"
#include "evaluator.h"
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <time.h>

using namespace Shared;
using namespace Environment;
using std::cout;
using std::cin;
using std::string;

namespace Evaluator
{
    Cell* buildGlobalBuiltins()
    {
        Cell* global = createEnv();
        srand(time(NULL));
        
        insert(const_cast<char*>("this"),
        		global,
				global);
        insertBuiltin("print",
                cons(Types::PARAMLIST, new Cell(Types::ID, const_cast<char*>("value")), 0),
                builtPrint,
                global);
        insertBuiltin("println",
                cons(Types::PARAMLIST, new Cell(Types::ID, const_cast<char*>("value")), 0),
                builtPrintln,
                global);
        insertBuiltin("import",
        		cons(Types::PARAMLIST, new Cell(Types::ID, const_cast<char*>("filename")), 0),
				builtImport,
				global);
        insertBuiltin("typeof",
        		cons(Types::PARAMLIST, new Cell(Types::ID, const_cast<char*>("object")),0),
				builtTypeOf,
				global);
        insertBuiltin("input",
                        0,
                        builtInput,
                        global);
        insertBuiltin("length",
        		cons(Types::PARAMLIST, new Cell(Types::ID, const_cast<char*>("object")), 0),
				builtLength,
				global);
		insertBuiltin("random",
		        cons(Types::PARAMLIST, new Cell(Types::ID, const_cast<char*>("min")),
		            cons(Types::PARAMLIST, new Cell(Types::ID, const_cast<char*>("max")),0)),
		        builtRandom,
		        global);
                                
        return global;
    }
    void insertBuiltin(const char* name, Cell* paramList, fptr func, Cell* env)
    {
        insert(const_cast<char*>(name),
                cons(Types::BUILT,
                    cons(Types::JOIN,
                            paramList,
                            new Cell(Types::BUILT, func)),
                    env),
                env);
    }
    
    Cell* builtPrint(Cell* tree, Cell* env)
    {
        Cell* value = tree->car();
        if (value == 0)
            return 0;
        else if (value->type == Types::INTEGER)
            cout << value->ivalue;
        else if (value->type == Types::STRING)
            cout << value->svalue;
        else
            cout << value->type << " Object";

        cout.flush();
            
        return value;
    }
    Cell* builtPrintln(Cell* tree, Cell* env)
    {
        Cell* val = builtPrint(tree, env);
        cout << "\n";
        return val;
    }
    Cell* builtImport(Cell* tree, Cell* env)
    {
    	if (tree->car() == 0 || tree->car()->type != Types::STRING)
    		return 0;

    	Cell* globEnv = env;
    	while (globEnv->car() != 0)
    		globEnv = globEnv->car();

    	char* filename = tree->car()->svalue;
    	Cell* importTree = Parse::parse(filename);
    	eval(importTree, globEnv);
    	return 0;
    }
    Cell* builtTypeOf(Cell* tree, Cell* env)
    {
    	if (tree->car() == 0)
    		return 0;
    	return new Cell(tree->car()->line, Types::STRING, tree->car()->type);
    }
    Cell* builtInput(Cell* tree, Cell* env)
    {
        string line;
        getline(cin, line);
        char* result = (char*)malloc(sizeof(char)*line.size()+1);
        strncpy(result, line.c_str(), line.size());
        *(result+line.size()) = 0;
        
        return new Cell(-1, Types::STRING, result);
    }
    Cell* builtLength(Cell* tree, Cell* env)
    {
    	if (tree->car() == 0)
    		return new Cell(-1, Types::INTEGER, 0);
    	else if (tree->car()->type == Types::STRING)
    	{
    		return new Cell(-1, Types::INTEGER, (signed int)strlen(tree->car()->svalue));
    	}
    	else if (tree->car()->type == Types::ARRAY)
    	{
    		return new Cell(-1, Types::INTEGER, (signed int)tree->car()->avalue->size());
    	}
    	else
    		return new Cell(-1, Types::INTEGER, -1);
    }
    Cell* builtRandom(Cell* tree, Cell* env)
    {
        int lower = (int)tree->car()->ivalue;
        int upper = (int)tree->cdr()->car()->ivalue;
        
        int val = rand() % (upper-lower) + lower;
        
        return new Cell(-1, Types::INTEGER, (long int)val);
    }
}
