/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * evalHelper.h
 */

#ifndef EVALHELPER
#define EVALHELPER

namespace Shared
{
	class Cell;
}
using Shared::Cell;

namespace Evaluator
{
	class EvalHelper
	{
	public:
		static int lineNum;
	};
	class EvalException
	{
	public:
		char* message;
		Cell* cell;
	};

    typedef Cell* (*fptr)(Cell*, Cell*);
    
    Cell* createClosure(Cell* paramList, Cell* statementList, Cell* env);
    bool truthful(Cell* tree);
    Cell* evalList(Cell* tree, Cell* env);
    
    Cell* getClosureParams(Cell* closure);
    Cell* getClosureBody(Cell* closure);
    Cell* getClosureEnv(Cell* closure);
    Cell* cleanClosureParams(Cell* params);
    
    char* plusType(Cell* exprList);
    
    Cell* getHelper(Cell* tree, Cell* env);

    fptr getEval(Cell* type);
}

#endif
