/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * evaluator.cpp
 */

#include "evaluator.h"

#include "evalHelper.h"
#include "builtin.h"
#include "../shared/cell.h"
#include "../shared/types.h"
#include "../environ/environ.h"
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <stdexcept>

using namespace Environment;
using namespace Shared;
using std::string;

namespace Evaluator
{
    Cell* eval(Cell* tree, Cell* env)
    {
        if (tree == 0)
            return 0;
        else
            return getEval(tree)(tree, env);
    }
    
    Cell* evalProgram(Cell* tree, Cell* env)
    {
        return eval(tree->car(), env);
    }
    
    Cell* evalPrimary(Cell* tree, Cell* env)
    {
        if (tree->type == Types::EXPR)
            tree = tree->car();
        if (tree->car() != 0)
        {
            //Not tag; reverse truthfulness of the primary
        	int line = EvalHelper::lineNum;
        	Cell* val;
        	if (tree->cdr()->type == Types::EXPR)
        		val = eval(tree->cdr(), env);
        	else
        		val = eval(new Cell(tree->car()->line, Types::PRIMARY, 0, tree->cdr()), env);
            if (truthful(val))
                return new Cell(line, Types::INTEGER, 0);
            else
                return new Cell(line, Types::INTEGER, 1);
        }
        
        Cell* what = (tree->type == Types::PRIMARY) ? tree->cdr() : tree;
        
        if (what->type == Types::INTEGER
            || what->type == Types::STRING)
        {
            return what;
        }
        else if (what->type == Types::ID || what->type == Types::OID)
        {
            Cell* val = lookup(what->svalue, env);
            if (val->type == Types::THUNK)
                return eval(val, env);
            else
                return val;
        }
        else if (what->type == Types::LAMBDA)
        {
            return eval(what, env);
        }
        else if (what->type == Types::FUNCTIONCALL)
        {
            return eval(what, env);
        }
        else if (what->type == Types::ARRAYREF)
        {
            return eval(what, env);
        }
        else if (what->type == Types::ARRAY)
        {
            return eval(what, env);
        }
        else if (what->type == Types::ONOT)
        {
        	int line = EvalHelper::lineNum;
            Cell* val = eval(new Cell(tree->car()->line, Types::PRIMARY, 0, tree->cdr()), env);
            if (truthful(val))
                return new Cell(line, Types::INTEGER, 0);
            else
                return new Cell(line, Types::INTEGER, 1);
        }
        else
        {
        	char message[50];
        	sprintf(message, "Parsing primary, but primary type is %s - line %d", what->type, tree->line);
        	throw std::runtime_error(message);
        }
            
        return 0;
    }
    
    Cell* evalStatementList(Cell* tree, Cell* env)
    {
        Cell* result = 0;
        Cell* list = tree;
        
        while (list != 0)
        {
            result = eval(list->car(), env);
            list = list->cdr();
        }
        
        return result;
    }
       
    Cell* evalDef(Cell* tree, Cell* env)
    {
		if (tree->car()->type == Types::ID)
		{
			//Normal def
			Cell* val = eval(tree->cdr(), env);
			insert(tree->car()->svalue, val, env);
		}
		else
		{
			//Function def
			Cell* clos = createClosure(tree->car()->cdr(),
										tree->cdr(),
										env);
			insert(tree->car()->car()->svalue, clos, env);
		}
		
		return 0;
	}

    Cell* evalSet(Cell* tree, Cell* env)
    {
    	int line = EvalHelper::lineNum;
    	Cell* toWhat = eval(tree->cdr(), env);
    	Cell* whoTree = tree->car()->car();
    	//Assert whoTree is primary
    	whoTree = whoTree->cdr();

    	if (whoTree->type == Types::ID)
    	{
    		return set(whoTree->svalue, toWhat, env);
    	}
    	else if (whoTree->type == Types::ARRAYREF)
    	{
    		//Passing in the first argument of the argRef
    		//This should return the object that we get stuff from
    		Cell* baseRef = getHelper(whoTree->car()->cdr(), env);
    		if (baseRef->type == Types::ENV)
    		{
    			Cell* selector;
    			if (whoTree->cdr()->type == Types::EXPR
					&& whoTree->cdr()->car()->type == Types::PRIMARY
					&& whoTree->cdr()->car()->cdr()->type == Types::ID)
					selector = whoTree->cdr()->car()->cdr();
				else
					selector = eval(whoTree->cdr(), env);
    			if (selector->type != Types::ID && selector->type != Types::STRING)
    			{
    				char message[50];
    				sprintf(message, "Invalid selector type %s - line %d", selector->type, selector->line);
    				throw std::runtime_error(message);
    			}
    			set(selector->svalue, toWhat, baseRef);
    			return toWhat;
    		}
    		else if (baseRef->type == Types::ARRAY)
    		{
    			Cell* selector = eval(whoTree->cdr(), env);
    			if (selector->type != Types::INTEGER)
    			{
    				char message[50];
    				sprintf(message, "Invalid selector type %s - line %d", selector->type, selector->line);
    				throw std::runtime_error(message);
    			}
    			if (selector->ivalue < 0 || (unsigned int)selector->ivalue >= baseRef->avalue->size())
    			{
    				char message[50];
    				sprintf(message, "Index out of bounds - line %d", selector->line);
    				throw std::runtime_error(message);
    			}
    			baseRef->avalue->at(selector->ivalue) = toWhat;
    			return toWhat;
    		}
    		else
    		{
    			char message[50];
    			sprintf(message, "Cannot get from type %s - line %d", baseRef->type, line);
    			throw std::runtime_error(message);
    		}
    	}
    	else
    	{
    		char message[70];
    		sprintf(message, "Cannot set type %s - line %d", whoTree->type, line);
    		throw std::runtime_error(message);
    	}
    }
	
	Cell* evalCall(Cell* tree, Cell* env)
	{
        Cell* closure = eval(tree->car(), env);
        if (closure->type != Types::CLOSURE && closure->type != Types::BUILT)
        {
        	char message[50];
        	sprintf(message, "Received type %s instead of closure - line %d", closure->type, tree->line);
        	throw std::runtime_error(message);
        }
        Cell* params = getClosureParams(closure);
        Cell* eArgs = evalArgs(tree->cdr(), params, env);
        
        Cell* body = getClosureBody(closure);
        Cell* funcEnv = getClosureEnv(closure);
        Cell* newEnv = extendEnv(cleanClosureParams(params), eArgs, funcEnv);
        insert(const_cast<char*>("this"), newEnv, newEnv);
        
        try
        {
        	Cell* result;
        	if (body->type == Types::BUILT)
        		result = body->fvalue(eArgs, newEnv);
        	else
        		result = eval(body, newEnv);
        	if (result != 0 && result->line<0)
        		result->line = tree->car()->line;
        	return result;
        }
        catch (Cell& e)
        {
        	Cell* result = new Cell(e);
        	if (result->type == Types::UNKNOWN)
        		return 0;
        	else
        		return result;
        }
	}
    
    Cell* evalArgs(Cell* args, Cell* params, Cell* env)
    {
        if (args == 0 && params == 0) return 0;
        else if (args == 0)
        {
        	char message[50];
        	sprintf(message, "Too few arguments - line %d", EvalHelper::lineNum);
        	throw std::runtime_error(message);
        }
        else if (params == 0)
        {
        	char message[70];
        	sprintf(message, "Too few parameters - line %d", EvalHelper::lineNum);
        	throw std::runtime_error(message);
        }
        else if (params->car()->type == Types::ODELAY)
        {
            return cons(args->car()->line,
            			Types::JOIN,
                        cons(args->car()->line, Types::THUNK, args->car(), env),
                        evalArgs(args->cdr(), params->cdr(), env));
        }
        else
        {
            return cons(args->car()->line,
            			Types::JOIN,
                        eval(args->car(), env),
                        evalArgs(args->cdr(), params->cdr(), env));
        }
    }
	
	Cell* evalLambda(Cell* tree, Cell* env)
	{
		return createClosure(tree->car(), tree->cdr(), env);
	}
	
	Cell* evalIf(Cell* tree, Cell* env)
	{
		Cell* cond = tree->car();
		while (cond != 0)
		{
			bool isElse = (cond->car()->car()->type == Types::OELSE);
			
			if (isElse)
			{
				return eval(cond->car()->cdr(), env);
			}
			else
			{
				Cell* val = eval(cond->car()->car(), env);
				if (truthful(val))
					return eval(cond->car()->cdr(), env);
			}
			
			cond = cond->cdr();
		}
		
		return 0;
	}
    
    Cell* evalWhileLoop(Cell* tree, Cell* env)
    {
        Cell* condition = tree->car();
        Cell* result = 0;
        
        while (truthful(eval(condition, env)))
        {
            result = eval(tree->cdr(), env);
        }
        
        return result;
    }
    
    Cell* evalBlock(Cell* tree, Cell* env)
    {
        if (tree->cdr() != 0)
        {
            Cell* newEnv = extendEnv(0, 0, env);
            return eval(tree->cdr(), newEnv);
        }
        return 0;
    }
    
    Cell* evalArray(Cell* tree, Cell* env)
    {
    	vector<Cell*>* items = new vector<Cell*>();
    	Cell* eItems = evalList(tree->car(), env);
    	int line = tree->line;
    	while (eItems != 0)
    	{
    		items->push_back(eItems->car());
    		eItems = eItems->cdr();
    	}

    	return new Cell(line, Types::ARRAY, items);
    }

    Cell* evalArrayRef(Cell* tree, Cell* env)
    {
    	if (tree->type == Types::ARRAYREF)
    	{
    		Cell* arr = eval(tree->car(), env);
    		if (arr->type == Types::ARRAY)
    		{
    			Cell* position = eval(tree->cdr(), env);
    			if (position==0 || position->type != Types::INTEGER)
    			{
    				char message[50];
    				sprintf(message, "Invalid array selector - line %d", arr->line);
    				throw std::runtime_error(message);
    			}
    			if (position->ivalue < 0 || position->ivalue > (signed int)arr->avalue->size())
    			{
    			    char message[50];
    			    sprintf(message, "Invalid array index %d - line %d", (int)position->ivalue, tree->line);
    			    throw std::runtime_error(message);
    			}
    			return arr->avalue->at(position->ivalue);
    		}
    		else if (arr->type == Types::ENV)
    		{
    			Cell* id = tree->cdr()->car()->cdr();
    			if (id->type != Types::ID && id->type != Types::STRING)
    			{
    				char message[70];
    				sprintf(message, "Invalid reference request of type %s - line %d", id->type, id->line);
    				throw std::runtime_error(message);
    			}
    			return lookup(id->svalue, arr);
    		}
    		else
    		{
    			char message[70];
    			sprintf(message, "Invalid reference request on type %s - line %d", arr->type, tree->line);
    			throw std::runtime_error(message);
    		}
    	}
    	else
    	{
    		char message[70];
    		sprintf(message, "Invalid reference request - line %d", tree->line);
    		throw std::runtime_error(message);
    	}
    }

    Cell* evalReturn(Cell* tree, Cell* env)
    {
    	int line  = EvalHelper::lineNum;
    	Cell* result = (tree->cdr()!=0) ? eval(tree->cdr(), env) : new Cell(line, Types::UNKNOWN);
    	const Cell r = *result;
    	throw r;
    }

    Cell* evalThunk(Cell* tree, Cell* env)
    {
    	return eval(tree->car(), tree->cdr());
    }

    Cell* evalPlus(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        char* type = plusType(eList);
        
        if (type == Types::INTEGER)
        {
            Cell* list = eList;
            long int val = 0;
            while (list != 0)
            {
                val += list->car()->ivalue;
                list = list->cdr();
            }
            return new Cell(lineNum, Types::INTEGER, val);
        }
        if (type == Types::STRING)
        {
            string ss;
            Cell* list = eList;
            while (list != 0)
            {
            	if (list->car()->type == Types::INTEGER)
            	{
            		char buff[33];
            		sprintf(buff,"%ld",list->car()->ivalue);
            		ss.append(buff);
            	}
            	else if (list->car()->type == Types::STRING)
            	{
            		ss.append(list->car()->svalue);
            	}
            	list = list->cdr();
            }
            char* cstr = new char[ss.length()+1];
            strncpy(cstr, ss.c_str(), ss.length());
            *(cstr+ss.length()) = 0;
            return new Cell(lineNum, Types::STRING, cstr);
        }
        if (type == Types::ARRAY)
        {
            //Append array
    	    vector<Cell*>* items = new vector<Cell*>();
            Cell* list = eList;
            while (list != 0)
            {
                if (list->car()->type == Types::ARRAY)
                {
                    vector<Cell*>* inner = list->car()->avalue;
                    int i;
                    for (i=0; i<(int)inner->size(); i++)
                    {
                        items->push_back(inner->at(i));
                    }
                }
                else
                {
                    items->push_back(list->car());
                }
                list = list->cdr();
            }
            return new Cell(lineNum, Types::ARRAY, items);
        }
        
        return 0;
    }
    
    Cell* evalMinus(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList->cdr();
        long int val = eList->car()->ivalue;
        
        while(list != 0)
        {
            val -= list->car()->ivalue;
            list = list->cdr();
        }
        
        return new Cell(lineNum, Types::INTEGER, val);
    }
    
    Cell* evalMultiply(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList->cdr();
        long int val = eList->car()->ivalue;
        
        while(list != 0)
        {
            val *= list->car()->ivalue;
            list = list->cdr();
        }
        
        return new Cell(lineNum, Types::INTEGER, val);
    }
    
    Cell* evalDivide(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList->cdr();
        long int val = eList->car()->ivalue;
        
        while(list != 0)
        {
            val /= list->car()->ivalue;
            list = list->cdr();
        }
        
        return new Cell(lineNum, Types::INTEGER, val);
    }
    
    Cell* evalModulo(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList->cdr();
        long int val = eList->car()->ivalue;
        val = (val<0) ? -val : val;
        long int nextVal;
        
        while(list != 0)
        {
            nextVal = list->car()->ivalue;
            nextVal = (nextVal<0) ? -nextVal : nextVal;
            val = val % nextVal;
            list = list->cdr();
        }
        
        return new Cell(lineNum, Types::INTEGER, val);
    }
    
    Cell* evalPtrEq(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList;
        while (list->cdr() != 0)
        {
            if (list->car() != list->cdr()->car())
                return new Cell(lineNum, Types::INTEGER, 0);
            list = list->cdr();
        }
        return new Cell(lineNum, Types::INTEGER, 1);
    }
    
    Cell* evalEq(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList;
        while (list->cdr() != 0)
        {
            if (list->car()->type == Types::INTEGER && list->cdr()->car()->type == Types::INTEGER)
            {
                if (list->car()->ivalue != list->cdr()->car()->ivalue)
                    return new Cell(lineNum, Types::INTEGER, 0);
            }
            else if (list->car()->type == Types::STRING && list->cdr()->car()->type == Types::STRING)
            {
            	if (strcmp(list->car()->svalue, list->cdr()->car()->svalue) != 0)
            		return new Cell(lineNum, Types::INTEGER, 0);
            }
            else
            {
            	char message[100];
            	sprintf(message, "Uncomparable objects %s and %s - line %d", list->car()->type, list->cdr()->car()->type, lineNum);
            	throw std::runtime_error(message);
            }
            list = list->cdr();
        }
        return new Cell(lineNum, Types::INTEGER, 1);
    }
    
    Cell* evalLt(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList;
        while (list->cdr() != 0)
        {
            if (list->car()->type == Types::INTEGER && list->cdr()->car()->type == Types::INTEGER)
            {
                if (list->car()->ivalue >= list->cdr()->car()->ivalue)
                    return new Cell(lineNum, Types::INTEGER, 0);
            }
            else if (list->car()->type == Types::STRING && list->cdr()->car()->type == Types::STRING)
            {
            	if (strcmp(list->car()->svalue, list->cdr()->car()->svalue) >= 0)
            		return new Cell(lineNum, Types::INTEGER, 0);
            }
            else
            {
            	char message[100];
            	sprintf(message, "Uncomparable objects %s and %s - line %d", list->car()->type, list->cdr()->car()->type, lineNum);
            	throw std::runtime_error(message);
            }
            list = list->cdr();
        }
        return new Cell(lineNum, Types::INTEGER, 1);
    }
    
    Cell* evalGt(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList;
        while (list->cdr() != 0)
        {
            if (list->car()->type == Types::INTEGER && list->cdr()->car()->type == Types::INTEGER)
            {
                if (list->car()->ivalue <= list->cdr()->car()->ivalue)
                    return new Cell(lineNum, Types::INTEGER, 0);
            }
            else if (list->car()->type == Types::STRING && list->cdr()->car()->type == Types::STRING)
            {
            	if (strcmp(list->car()->svalue, list->cdr()->car()->svalue) <= 0)
            		return new Cell(lineNum, Types::INTEGER, 0);
            }
            else
            {
            	char message[100];
            	sprintf(message, "Uncomparable objects %s and %s - line %d", list->car()->type, list->cdr()->car()->type, lineNum);
            	throw std::runtime_error(message);
            }
            list = list->cdr();
        }
        return new Cell(lineNum, Types::INTEGER, 1);
    }
    
    Cell* evalLe(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList;
        while (list->cdr() != 0)
        {
            if (list->car()->type == Types::INTEGER && list->cdr()->car()->type == Types::INTEGER)
            {
                if (list->car()->ivalue > list->cdr()->car()->ivalue)
                    return new Cell(lineNum, Types::INTEGER, 0);
            }
            else if (list->car()->type == Types::STRING && list->cdr()->car()->type == Types::STRING)
            {
            	if (strcmp(list->car()->svalue, list->cdr()->car()->svalue) > 0)
            		return new Cell(lineNum, Types::INTEGER, 0);
            }
            else
            {
            	char message[100];
            	sprintf(message, "Uncomparable objects %s and %s - line %d", list->car()->type, list->cdr()->car()->type, lineNum);
            	throw std::runtime_error(message);
            }
            list = list->cdr();
        }
        return new Cell(lineNum, Types::INTEGER, 1);
    }
    
    Cell* evalGe(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList;
        while (list->cdr() != 0)
        {
            if (list->car()->type == Types::INTEGER && list->cdr()->car()->type == Types::INTEGER)
            {
                if (list->car()->ivalue < list->cdr()->car()->ivalue)
                    return new Cell(lineNum, Types::INTEGER, 0);
            }
            else if (list->car()->type == Types::STRING && list->cdr()->car()->type == Types::STRING)
            {
            	if (strcmp(list->car()->svalue, list->cdr()->car()->svalue) < 0)
            		return new Cell(lineNum, Types::INTEGER, 0);
            }
            else
            {
            	char message[100];
            	sprintf(message, "Uncomparable objects %s and %s - line %d", list->car()->type, list->cdr()->car()->type, lineNum);
            	throw std::runtime_error(message);
            }
            list = list->cdr();
        }
        return new Cell(lineNum, Types::INTEGER, 1);
    }
    
    Cell* evalNe(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* eList = evalList(tree->cdr(), env);
        
        Cell* list = eList;
        while (list->cdr() != 0)
        {
            if (list->car()->type == Types::INTEGER && list->cdr()->car()->type == Types::INTEGER)
            {
                if (list->car()->ivalue == list->cdr()->car()->ivalue)
                    return new Cell(lineNum, Types::INTEGER, 0);
            }
            else if (list->car()->type == Types::STRING && list->cdr()->car()->type == Types::STRING)
            {
            	if (strcmp(list->car()->svalue, list->cdr()->car()->svalue) == 0)
            		return new Cell(lineNum, Types::INTEGER, 0);
            }
            else
            {
            	char message[100];
            	sprintf(message, "Uncomparable objects %s and %s - line %d", list->car()->type, list->cdr()->car()->type, lineNum);
            	throw std::runtime_error(message);
            }
            list = list->cdr();
        }
        return new Cell(lineNum, Types::INTEGER, 1);
    }
    
    Cell* evalAnd(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* list = tree->cdr();
        
        while (list != 0)
        {
            Cell* evaled = eval(list->car(), env);
            if (!truthful(evaled))
                return new Cell(lineNum, Types::INTEGER, 0);
            list = list->cdr();
        }
        return new Cell(lineNum, Types::INTEGER, 1);
    }
    
    Cell* evalOr(Cell* tree, Cell* env)
    {
    	int lineNum = EvalHelper::lineNum;
        Cell* list = tree->cdr();
        
        while (list != 0)
        {
            Cell* evaled = eval(list->car(), env);
            if (truthful(evaled))
                return new Cell(lineNum, Types::INTEGER, 1);
            list = list->cdr();
        }
        return new Cell(lineNum, Types::INTEGER, 0);
    }
}
