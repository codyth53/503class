/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * evalHelper.cpp
 */

#include "evalHelper.h"
#include "evaluator.h"
#include "../shared/cell.h"
#include "../shared/types.h"
#include "../environ/environ.h"
#include <stdio.h>
#include <stdexcept>

using namespace Shared;
using namespace Environment;

namespace Evaluator
{
	int EvalHelper::lineNum = -1;

    Cell* createClosure(Cell* paramList, Cell* statementList, Cell* env)
    {
    	return cons(EvalHelper::lineNum,
			Types::CLOSURE,
			cons(EvalHelper::lineNum, Types::JOIN, paramList, statementList),
			env);
	}
	
	bool truthful(Cell* tree)
	{
		if (tree->type == Types::INTEGER && tree->ivalue == 0)
			return false;
		return true;
	}
    
    Cell* evalList(Cell* tree, Cell* env)
    {
        if (tree == 0)
            return 0;
        Cell* val = eval(tree->car(), env);
        return cons(val->line, Types::JOIN, val, evalList(tree->cdr(), env));
    }
 
    Cell* getClosureParams(Cell* closure)
    {
        return closure->car()->car();
    }
    Cell* getClosureBody(Cell* closure)
    {
        return closure->car()->cdr();
    }
    Cell* getClosureEnv(Cell* closure)
    {
        return closure->cdr();
    }
    Cell* cleanClosureParams(Cell* params)
    {
    	if (params == 0)
    		return 0;
    	else
    	{
    		if (params->car()->type == Types::ODELAY)
    		{
    			return cons(params->car()->car()->line, params->type, params->car()->car(), cleanClosureParams(params->cdr()));
    		}
    		else
    		{
    			return cons(params->car()->line, params->type, params->car(), cleanClosureParams(params->cdr()));
    		}
    	}
    }
    
    char* plusType(Cell* exprList)
    {
        //exprList should already be evaluated
        char* thisType = exprList->car()->type;
        char* nextType = (exprList->cdr() != 0) ? plusType(exprList->cdr()) : Types::INTEGER;
        
        if (thisType == Types::INTEGER
            && nextType == Types::INTEGER)
            return Types::INTEGER;
        else if ((thisType == Types::STRING
        			|| thisType == Types::INTEGER)
                    && (nextType == Types::STRING
                        || nextType == Types::INTEGER))
            return Types::STRING;
        else if (thisType == Types::ARRAY
                    || nextType == Types::ARRAY)
            return Types::ARRAY;
        else
            return Types::UNKNOWN;
    }

    Cell* getHelper(Cell* tree, Cell* env)
    {
    	if (tree->type == Types::ID)
    	{
    		return lookup(tree->svalue, env);
    	}
    	if (tree->type == Types::ARRAYREF)
    	{
    		Cell* next = tree->car()->cdr();
    		if (next->type == Types::ARRAYREF)
    			next = getHelper(next, env);
    		if (next->type == Types::ID)
    		{
    			next = lookup(next->svalue, env);
    		}
    		if (next->type == Types::ENV)
    		{
    			if (tree->cdr()->type == Types::EXPR
    				&& tree->cdr()->car()->type == Types::PRIMARY
					&& tree->cdr()->car()->cdr()->type == Types::ID)
    				return lookup(tree->cdr()->car()->cdr()->svalue, next);
    			else
    				return lookup(eval(tree->cdr(), env)->svalue, next);
    		}
    		else if (next->type == Types::ARRAY)
    		{
    			Cell* index = eval(tree->cdr(), env);
    			if (index->type == Types::INTEGER)
    			{
    				if (index->ivalue >= 0 && (unsigned int)index->ivalue < next->avalue->size())
    					return next->avalue->at(index->ivalue);
    				else
    				{
    					char message[50];
    					sprintf(message, "Index out of bounds - line %d", index->line);
    					throw std::runtime_error(message);
    				}
    			}
    			else
    			{
    				char message[50];
    				sprintf(message, "Invalid indexer type of %s - line %d", index->type, index->line);
    				throw std::runtime_error(message);
    			}
    		}
    		/*else if (next->type == Types::ID)
    		{
    			return lookup(next->svalue, env);
    		}*/
    		else
    		{
    			char message[50];
    			sprintf(message, "Invalid type %s to get from - line %d", next->type, next->line);
    			throw std::runtime_error(message);
    		}
    	}
    	else
    	{
    		char message[50];
    		sprintf(message, "Invalid type %s to get from - line %d", tree->type, tree->line);
    		throw std::runtime_error(message);
    	}
    }

    fptr getEval(Cell* tree)
    {
    	EvalHelper::lineNum = tree->line;

        //cout << "Getting type " << tree->type << "\n";
        if (tree->type == Types::PROGRAM)
            return evalProgram;
        else if (tree->type == Types::PRIMARY)
            return evalPrimary;
        else if (tree->type == Types::STATEMENTLIST)
            return evalStatementList;
        else if (tree->type == Types::VARDEF)
            return evalDef;
        else if (tree->type == Types::SET)
            return evalSet;
        else if (tree->type == Types::FUNCTIONCALL)
            return evalCall;
        else if (tree->type == Types::LAMBDA)
            return evalLambda;
        else if (tree->type == Types::IFSTATEMENT)
            return evalIf;
        else if (tree->type == Types::WHILELOOP)
            return evalWhileLoop;
        else if (tree->type == Types::BLOCK)
            return evalBlock;
        else if (tree->type == Types::ARRAY)
        	return evalArray;
        else if (tree->type == Types::ARRAYREF)
        	return evalArrayRef;
        else if (tree->type == Types::OID)
            return evalPrimary;
        else if (tree->type == Types::ORETURN)
        	return evalReturn;
        else if (tree->type == Types::THUNK)
        	return evalThunk;
        else if (tree->type == Types::EXPR)
        {
            Cell* what = tree->car();
            //cout << "Expr is " << what->type << "\n";
            if (what->type == Types::PRIMARY)
                return evalPrimary;
            else if (what->type == Types::OPLUS)
                return evalPlus;
            else if (what->type == Types::OMINUS)
                return evalMinus;
            else if (what->type == Types::OMULTIPLY)
                return evalMultiply;
            else if (what->type == Types::ODIVIDE)
                return evalDivide;
            else if (what->type == Types::OMODULO)
                return evalModulo;
            else if (what->type == Types::OPTREQ)
                return evalPtrEq;
            else if (what->type == Types::OEQ)
                return evalEq;
            else if (what->type == Types::OLT)
                return evalLt;
            else if (what->type == Types::OGT)
                return evalGt;
            else if (what->type == Types::OLE)
                return evalLe;
            else if (what->type == Types::OGE)
                return evalGe;
            else if (what->type == Types::ONE)
                return evalNe;
            else if (what->type == Types::OAND)
                return evalAnd;
            else if (what->type == Types::OOR)
                return evalOr;
            else
            {
            	char message[50];
            	sprintf(message, "Found invalid expression operator \"%s\" - line %d", what->type, EvalHelper::lineNum);
            	throw std::runtime_error(message);
            }
        }
        else
        {
        	char message[50];
        	sprintf(message, "Found invalid evaluating object - line %d", EvalHelper::lineNum);
        	throw std::runtime_error(message);
        }
    }
}
