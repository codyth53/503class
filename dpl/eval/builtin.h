/* Cody Henderson
 * Spring 2016
 * XML Language Souce
 * builtin.h
 */

#ifndef BUILTIN
#define BUILTIN

namespace Shared
{
	class Cell;
}
using Shared::Cell;

namespace Evaluator
{
    typedef Cell* (*fptr)(Cell*, Cell*);
    
    Cell* buildGlobalBuiltins();
    void insertBuiltin(const char* name, Cell* paramList, fptr func, Cell* env);
    
    Cell* builtPrint(Cell* tree, Cell* env);
    Cell* builtPrintln(Cell* tree, Cell* env);
    Cell* builtImport(Cell* tree, Cell* env);
    Cell* builtTypeOf(Cell* tree, Cell* env);
    Cell* builtInput(Cell* tree, Cell* env);
    Cell* builtLength(Cell* tree, Cell* env);
    Cell* builtRandom(Cell* tree, Cell* env);
}
#endif
